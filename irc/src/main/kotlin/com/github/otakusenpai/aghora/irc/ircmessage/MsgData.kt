package com.github.otakusenpai.aghora.irc.ircmessage

import com.github.otakusenpai.aghora.irc.PacketType

class MsgData() {

    constructor(prefix: Prefix,command: String,sender: String,content: String,packet: PacketType): this() {
        this.prefix = prefix
        this.command = command
        this.sender = sender
        this.content = content
        this.packet = packet
    }

    lateinit var prefix: Prefix
    lateinit var command: String
    lateinit var sender: String
    lateinit var content: String
    lateinit var packet: PacketType
}