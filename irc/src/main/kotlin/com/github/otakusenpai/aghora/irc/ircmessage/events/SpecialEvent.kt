package com.github.otakusenpai.aghora.irc.ircmessage.events

import com.github.otakusenpai.aghora.commonUtils.hasIt
import com.github.otakusenpai.aghora.irc.MsgType
import com.github.otakusenpai.aghora.irc.PacketType
import com.github.otakusenpai.aghora.irc.ircmessage.*
import com.github.otakusenpai.aghora.irc.retPacketType


// :hitchcock.freenode.net 250 G33kb0i :Highest connection count: 5964 (5963 clients) (1166921 connections received)
// :hitchcock.freenode.net 255 G33kb0i :I have 4330 clients and 1 servers

// First kind
// :hitchcock.freenode.net 004 G33kb0i hitchcock.freenode.net ircd-seven-1.1.5 DOQRSZaghilopswz CFILMPQSbcefgijklmnopqrstvz bkloveqjfI
// :hitchcock.freenode.net 252 G33kb0i 26 :IRC Operators online

// Second Kind
// :hitchcock.freenode.net 372 G33kb0i :- freenode runs an open proxy scanner.

class SpecialEvent(value: MsgType) : Event(value) {
    override fun onNotify(data: String,msgtype: MsgType): MsgData {
        lateinit var msg: MsgData
        lateinit var prefix: Prefix
        lateinit var command: String
        lateinit var packet: PacketType
        lateinit var sender: String
        lateinit var content: String

        try {
            if (this.msg == msgtype) {

                if(hasIt(data,"004") || hasIt(data,"005") ||
                   hasIt(data,"252") || hasIt(data,"253") ||
                   hasIt(data,"265") || hasIt(data,"266") ||
                   hasIt(data,"254")) {

                    var wordList = data.split("\\s{1,}".toRegex())
                    prefix = retPrefix(wordList.get(0))
                    command = wordList.get(1)
                    packet = retPacketType(command.toInt())
                    sender = wordList.get(2)

                    var temp: String = ""
                    var myList = wordList.drop(3)
                    for(i in myList) temp +=i
                    content = temp
                } else if(hasIt(data,"001") || hasIt(data,"002") ||
                     hasIt(data,"003") || hasIt(data,"251") ||
                     hasIt(data,"255") || hasIt(data,"250") ||
                     hasIt(data,"375") || hasIt(data,"372") ||
                     hasIt(data,"376")) {

                      content = data.substring(data.indexOf(":"), data.length)
                      var others = data.substring(0, data.indexOf(":"))

                      var otherList = others.split("\\s{1,}".toRegex())
                      prefix = retPrefix(otherList.get(0))

                      command = otherList.get(1)
                      sender = otherList.get(2)

                      packet = retPacketType(command.toInt())
                }
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        }
        msg = MsgData(prefix, command, sender, content, packet)
        return msg
    }
}