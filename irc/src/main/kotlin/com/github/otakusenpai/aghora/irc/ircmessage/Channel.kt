package com.github.otakusenpai.aghora.irc.ircmessage

class Channel() {

    constructor(chan: String) : this() {
        this.name = chan
    }

    constructor(chan: Channel) : this() {
        this.name = chan.name
        this.userList = chan.userList.toMutableList()
        this.mode =  chan.mode
        this.topic = chan.topic
    }

    fun userInChannel(nick: String) : Boolean = userList.any { it.nick == nick }

    fun setChanMode(value: String) {
        for((i,c) in value.toCharArray().withIndex()) {
            if(c == '+')
                continue
            else
                mode.set(i,c)
        }
    }

    fun setTopic(data: String) {
        topic = data
    }

    fun addUserToChan(msg: IRCMessage) {
        if(msg.retSender().equals(this.name)) {
            userList.add(msg.retPrefix())
        }
    }

    fun getPrefix(nick: String) : Prefix {
        lateinit var prefix: Prefix
        var found  = false
        for(i in userList) {
            if(i.nick == nick) {
                found = true
                prefix = Prefix(i)
            }
        }
        if(!found)
            throw Exception("Did not find $nick in $name")
        else
            return prefix
    }

    fun retName() = name
    fun retTopic() = topic
    fun retMode() = mode
    fun retUserList() = userList

    private var name: String = ""
    private var topic: String = ""
    private var mode: CharArray = charArrayOf()
    var userList: MutableList<Prefix> = mutableListOf()
}
