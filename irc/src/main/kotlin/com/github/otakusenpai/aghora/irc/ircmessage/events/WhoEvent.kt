package com.github.otakusenpai.aghora.irc.ircmessage.events

import com.github.otakusenpai.aghora.commonUtils.isNumeric
import com.github.otakusenpai.aghora.irc.MsgType
import com.github.otakusenpai.aghora.irc.PacketType
import com.github.otakusenpai.aghora.irc.ircmessage.*
import com.github.otakusenpai.aghora.irc.retPacketType

// :weber.freenode.net 352 BamBaka ##helloworlders ~qq unaffiliated/mahjong leguin.freenode.net Mahjong H+ :0 realname
// :weber.freenode.net 352 BamBaka ##helloworlders notohnx unaffiliated/ohnx barjavel.freenode.net ohnx G+ :0 ohnx
// :cherryh.freenode.net 352 BamBaka ##llamas ~bspar 2604:a880:0:1010::776:e001 rajaniemi.freenode.net bspar H :0 bspar
// :orwell.freenode.net 352 BamBaka ##ChatterZ ~SmokinGru 162-226-245-243.lightspeed.cicril.sbcglobal.net card.freenode.net Aztec03 H :0 Feel

class WhoEvent(value: MsgType) : Event(value) {
    override fun onNotify(data: String,msgtype: MsgType): MsgData {
        var msg: MsgData
        var content: String = ""
        lateinit var prefix: Prefix
        var command: String = ""
        var sender: String = ""
        lateinit var packet: PacketType
        var nick: String = ""
        var user = ""
        var host = ""

        try {
            if (this.msg == msgtype) {
                val msgList = data.split("\\s{1,}".toRegex())
                nick = msgList.get(7)
                user = msgList.get(4)
                host = msgList.get(5)
                prefix = retPrefix("$nick!$user@$host")
                command = msgList.get(1)
                sender = msgList.get(3)

                val contentList = msgList.subList(9,msgList.size)
                for(i in contentList)
                    content = content + i
                if (isNumeric(command)) {
                    packet = retPacketType(command.toInt())
                } else packet = PacketType.OTHER
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        }
        msg = MsgData(prefix, command, sender, content, packet)

        return msg
    }
}