package com.github.otakusenpai.aghora.irc

import com.github.otakusenpai.aghora.irc.connection.Connection
import com.github.otakusenpai.aghora.irc.ircmessage.Channel
import com.github.otakusenpai.aghora.irc.ircmessage.IRCMessage

enum class IRCCommands(val irccommand: Int) {
    None(-1),
    SendBan(0),
    SendBanwithExcempt(1),
    SendUnban(2),
    SendUnbanwithExcempt(3),
    JoinChannel(4),
    SendMsg(5),
    SendNick(6),
    SendUser(7),
    SendMe(8),
    SendPrivMsg(9),
    SendNotice(10),
    SendPong(11),
    SendPart(12),
    SendNickservIdentify(13),
    SendWho(14),
    SendKick(15),
    SendInvite(16),
    SendMode(17),
    SendChannelLimit(18);

    override fun toString(): String = this.irccommand.toString()
}

fun setIRCCommand(command: String): IRCCommands {
    var retData: IRCCommands
    if(command == "SendBan")
        retData = IRCCommands.SendBan
    else if(command == "SendBanwithExcempt")
        retData = IRCCommands.SendBanwithExcempt
    else if(command == IRCCommands.SendUnban.toString())
        retData = IRCCommands.SendUnban
    else if(command == IRCCommands.SendBanwithExcempt.toString())
        retData = IRCCommands.SendBanwithExcempt
    else if(command == IRCCommands.JoinChannel.toString())
        retData = IRCCommands.JoinChannel
    else if(command == IRCCommands.SendMsg.toString())
        retData = IRCCommands.SendMsg
    else if(command == IRCCommands.SendNick.toString())
        retData = IRCCommands.SendNick
    else if(command == IRCCommands.SendUser.toString())
        retData = IRCCommands.SendUser
    else if(command == IRCCommands.SendMe.toString())
        retData = IRCCommands.SendMe
    else if(command == IRCCommands.SendPrivMsg.toString())
        retData = IRCCommands.SendPrivMsg
    else if(command == IRCCommands.SendNotice.toString())
        retData = IRCCommands.SendNotice
    else if(command == IRCCommands.SendPong.toString())
        retData = IRCCommands.SendPong
    else if(command == IRCCommands.SendPart.toString())
        retData = IRCCommands.SendPart
    else if(command == IRCCommands.SendNickservIdentify.toString())
        retData = IRCCommands.SendNickservIdentify
    else if(command == IRCCommands.SendWho.toString())
        retData = IRCCommands.SendWho
    else if(command == IRCCommands.SendKick.toString())
        retData = IRCCommands.SendKick
    else if(command == IRCCommands.SendInvite.toString())
        retData = IRCCommands.SendInvite
    else if(command == IRCCommands.SendMode.toString())
        retData = IRCCommands.SendMode
    else if(command == IRCCommands.SendChannelLimit.toString())
        retData = IRCCommands.SendChannelLimit
    else if(command == "")
        retData = IRCCommands.None
    else
        throw Throwable("Error: Command not found at function setIRCCommand !")
    return retData
}

fun JoinChannel(chan: Channel, conn: Connection) {
    var temp = "JOIN " + chan.retName() + "\r\n"
    conn.sendData(temp)
}

fun JoinChannel(channel: String,conn: Connection) {
    var temp = "JOIN $channel\r\n"
    conn.sendData(temp)
}
fun JoinChannelQueue(channel: String): Pair<String,String> = Pair(channel,"JOIN $channel\r\n")

//Send a normal message
fun SendRawMsg(msg: String, conn: Connection) {
    conn.sendData(msg)
}

fun SendRawMsgQueue(target: String,msg: String): Pair<String,String> = Pair(target,msg)

fun SendNick(botName: String,conn: Connection) {
    conn.sendData("NICK $botName\r\n")
}

//Send USER message
fun SendUser(user: String,realname: String,mode: Int,conn: Connection) {
    var s = "USER " + user + " " + mode.toString() + " * :" + realname + "\r\n"
    conn.sendData(s)
}

//Me message
fun SendMe(target: String, message: String, conn: Connection) {
    var s = "PRIVMSG $target :\u0001ACTION $message\u0001\r\n"
    conn.sendData(s)
}

fun SendMeQueue(target: String, message: String): Pair<String,String> =
        Pair(target, "PRIVMSG $target :\u0001ACTION $message\u0001\r\n")

//PrivMsg message
fun SendPrivMsg(target: String,message: String, conn: Connection) {
    var temp = "PRIVMSG $target :$message\r\n"
    conn.sendData(temp)
}

fun SendPrivMsgQueue(target: String,message: String): Pair<String,String> =
        Pair(target, "PRIVMSG $target :$message\r\n")

fun SendNotice(target: String,message: String, conn: Connection) {
    var temp = "NOTICE $target :$message\r\n"
    println(temp)
    conn.sendData(temp)
}

fun SendNoticeQueue(target: String,message: String): Pair<String,String> =
        Pair(target,"NOTICE $target :$message\r\n")

//Send PONG message
fun SendPong(contents: String,conn: Connection) {
    var temp = "PONG $contents\r\n"
    conn.sendData(temp)
}

//Part message
fun SendPart(channel: String, conn: Connection) {
    var temp = "PART $channel\r\n"
    conn.sendData(temp)
}
fun SendPart(channel: String, message: String, conn: Connection) {
    var temp = "PART $channel :$message\r\n"
    conn.sendData(temp)
}

fun SendPartQueue(channel: String): Pair<String,String> = Pair(channel, "PART $channel\r\n")


fun SendPartQueue(channel: String, message: String): Pair<String,String> =
        Pair(channel,"PART $channel :$message\r\n")

fun SendNickservIdentify(password: String, conn: Connection) {
    var s = "PRIVMSG NICKSERV :IDENTIFY $password\r\n"
    conn.sendData(s)
}

fun SendWho(channel: String, conn: Connection) {
    val s = "WHO $channel\r\n"
    conn.sendData(s)
}

fun SendWho(channel: String) : String = "WHO $channel\r\n"

fun SendWhoQueue(channel: String): Pair<String,String> =
        Pair(channel, "WHO $channel\r\n")

fun SendKick(channel: Channel, nick: String, conn: Connection): Boolean {
    var sent = false
    if(channel.userInChannel(nick)) {
        val s = "KICK " + channel.retName() + " " + nick + "\r\n"
        conn.sendData(s)
        sent = true
    }
    return sent
}

fun SendKick(channel: String, nick: String): String = "KICK $channel $nick\r\n"

fun SendKick(channel: Channel, nick: String, msg: String, conn: Connection): Boolean {
    var sent = false
    if(channel.userInChannel(nick)) {
        val s = "KICK " + channel.retName() + " " + nick + " :" + msg + "\r\n"
        conn.sendData(s)
        sent = true
    }
    return sent
}

fun SendKick(channel: String, nick: String, msg: String): String = "KICK $channel $nick :$msg\r\n"
fun SendKickQueue(channel: String, nick: String): Pair<String,String> =
        Pair(channel,"KICK $channel $nick\r\n")
fun SendKickQueue(channel: String, nick: String, msg: String): Pair<String,String> =
        Pair(channel, "KICK $channel $nick :$msg\r\n")

fun SendInvite(channel: String,nick: String): String {
    var s = "INVITE $nick $channel\r\n"
    return s
}

fun SendInviteQueue(channel: String,nick: String): Pair<String,String> =
        Pair(channel, "INVITE $nick $channel\r\n")

fun SendMode(channel: String,mode: Char,flag: Boolean): String {
    var s = ""
    if(flag)
        s = "MODE $channel +$mode\r\n"
    else
        s = "MODE $channel -$mode\r\n"
    return s
}

fun SendModeQueue(channel: String,mode: Char,flag: Boolean): Pair<String,String> {
    lateinit var pair: Pair<String,String>
    if(flag)
        pair = Pair(channel, "MODE $channel +$mode\r\n")
    else
        pair = Pair(channel, "MODE $channel -$mode\r\n")
    return pair
}

fun SendMode(channel: String,nick: String,mode: Char,flag: Boolean): String {
    var s = ""
    if(flag)
        s = "MODE $channel +$mode $nick\r\n"
    else
        s = "MODE $channel -$mode $nick\r\n"
    return s
}

fun SendModeQueue(channel: String,nick: String,mode: Char,flag: Boolean): Pair<String,String> {
    lateinit var pair: Pair<String,String>
    if(flag)
        pair = Pair(channel, "MODE $channel +$mode $nick\r\n")
    else
        pair = Pair(channel, "MODE $channel -$mode $nick\r\n")
    return pair
}

fun SendChannelLimit(channel: String,number: Int): String = "MODE $channel +l $number\r\n"

fun SendBan(channel: String,prefix: String) = "MODE $channel +b $prefix\r\n"
fun SendBanwithExcept(channel: String,prefix: String,
                      except: String) = "MODE $channel +be $prefix $except\r\n"

fun SendUnban(channel: String,prefix: String) = "MODE $channel -b $prefix\r\n"
fun SendUnbanwithExcept(channel: String,prefix: String,
                      except: String) = "MODE $channel -be $prefix $except\r\n"