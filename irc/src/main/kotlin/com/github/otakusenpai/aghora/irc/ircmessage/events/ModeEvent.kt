package com.github.otakusenpai.aghora.irc.ircmessage.events

import com.github.otakusenpai.aghora.commonUtils.hasIt
import com.github.otakusenpai.aghora.irc.MsgType
import com.github.otakusenpai.aghora.irc.PacketType
import com.github.otakusenpai.aghora.irc.ircmessage.*


// :ChanServ!ChanServ@services. MODE ##helloworlders +v G33kb0i
// :G33kb0i MODE G33kb0i :+i
// :handicraftsman!~handicraf@c.handicraftsman.tk MODE ##helloworlders -b *!*@freebsd/user/detectivetaco

class ModeEvent(value: MsgType) : Event(value) {
    override fun onNotify(data: String, msgtype: MsgType): MsgData {
        lateinit var msg: MsgData
        var content: String = ""
        lateinit var prefix: Prefix
        lateinit var command: String
        lateinit var sender: String
        lateinit var packet: PacketType

        try {
            if (this.msg == msgtype) {
                var msgList = data.split("\\s{1,}".toRegex())

                prefix = retPrefix(msgList.get(0))

                command = msgList[1]
                sender = msgList[2]

                if(hasIt(msgList.get(3),'+') || hasIt(msgList.get(3),'-')) {
                    if(msgList.size > 4) {
                        val contentList = msgList.subList(3,msgList.size)
                        for(i in contentList)
                            content = content + " " + i
                    } else content = msgList.get(3)
                }

                packet = PacketType.OTHER
            }
        } catch (e: Throwable) {
            e.printStackTrace()
        }
        msg = MsgData(prefix, command, sender, content, packet)

        return msg
    }
}
