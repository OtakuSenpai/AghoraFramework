package com.github.otakusenpai.aghora.irc.ircmessage.events

import com.github.otakusenpai.aghora.irc.MsgType
import com.github.otakusenpai.aghora.irc.PacketType
import com.github.otakusenpai.aghora.irc.ircmessage.*
import kotlin.text.*

// :card.freenode.net NOTICE * :*** Looking up your hostname...

class NoticeEvent(value: MsgType) : Event(value) {
    override fun onNotify(data: String,msgtype: MsgType): MsgData {
        lateinit var msg: MsgData
        lateinit var content: String
        lateinit var prefix: Prefix
        lateinit var command: String
        lateinit var sender: String
        lateinit var packet: PacketType


        try {
            if (this.msg == msgtype) {
                var dataList = data.split("\\s{1,}".toRegex())
                content = data.substring(data.indexOf(dataList.get(2)) +
                        dataList.get(2).length, data.length)
                var others = data.substring(0, data.indexOf(dataList.get(2)) + dataList.get(2).length)
                var otherList = others.split("\\s{1,}".toRegex())

                prefix = retPrefix(otherList.get(0))
                command = otherList.get(1)
                sender = otherList.get(2)

                packet = PacketType.OTHER
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        }
        msg = MsgData(prefix, command, sender, content, packet)

        return msg
    }
}