package com.github.otakusenpai.aghora.irc.ircmessage

import com.github.otakusenpai.aghora.commonUtils.error.AghoraException
import com.github.otakusenpai.aghora.commonUtils.hasIt

class Prefix() {

    constructor(Nick: String,User: String,Host: String,Type: PrefixType) : this() {
        this.user = User
        this.nick = Nick
        this.hostname = Host
        this.prefixType = Type
    }

    constructor(data: Prefix) : this() {
        this.nick = data.nick
        this.hostname = data.hostname
        this.prefixType = data.prefixType
        this.user = data.user
    }

    constructor(data: String) : this(retPrefix(data))

    fun getData(): String = when(prefixType) {
        PrefixType.Normal-> "$nick!$user@$hostname"
        PrefixType.Host -> hostname
        PrefixType.Nick -> nick
        PrefixType.None -> ""
    }

    fun clear() {
        this.nick = " "
        this.user = " "
        this.hostname = " "
    }

    enum class PrefixType(val type: Int) {
        Host(1),
        Normal(2),
        Nick(3),
        None(4)
    }

    lateinit var prefixType: PrefixType
    var nick: String = ""
    var user: String = ""
    var hostname: String = ""
}

val domainSuffixes = listOf<String>("com","net","org")

fun <String> List<String>.foundMatch(data: String): Boolean {
    var found = false
    for(i in this) {
        if(i == data) {
            found = true
            break
        }
    }
    return found
}

fun retPrefix(data: String): Prefix {
    var temp = data
    // Nick field of the prefix
    lateinit var nick: String
    // User field of the prefix
    lateinit var user: String
    // Host field of the prefix
    lateinit var host: String
    // Object to be instantiated and returned
    lateinit var prefix: Prefix
    // PrefixType
    lateinit var type: Prefix.PrefixType
    // 1: Is only host
    // 2: Is normal prefix
    // 3: Is only nick

    // host only
    if(domainSuffixes.foundMatch(data.substring((data.length - 3),data.length)) &&
            !hasIt(data,'!') && !hasIt(data,'@')) {
        host = temp
        nick = " "
        user = " "
        type = Prefix.PrefixType.Host

    }
    // Normal prefix
    else if((data.substring((data.length - 3),data.length) == "net") &&
            hasIt(data,'!') && hasIt(data,'@')) {
        nick = temp.substring(0,temp.indexOf("!"))
        temp = temp.substring(temp.indexOf("!") + 1, temp.length)
        user = temp.substring(0,temp.indexOf("@"))
        temp = temp.substring(temp.indexOf("@") + 1, temp.length)
        host = temp
        type = Prefix.PrefixType.Normal

    }
    // IPv6 prefix
    else if(hasIt(data,':')) {
        nick = temp.substring(0,temp.indexOf("!"))
        temp = temp.substring(temp.indexOf("!") + 1, temp.length)
        user = temp.substring(0,temp.indexOf("@"))
        temp = temp.substring(temp.indexOf("@") + 1, temp.length)
        host = temp
        type = Prefix.PrefixType.Normal
    }
    // Prefix without a host
    else if(hasIt(data,'!') && hasIt(data,'@')) {
        nick = temp.substring(0,temp.indexOf("!"))
        temp = temp.substring(temp.indexOf("!") + 1, temp.length)
        user = temp.substring(0,temp.indexOf("@"))
        temp = temp.substring(temp.indexOf("@") + 1, temp.length)
        host = temp
        type = Prefix.PrefixType.Normal
    }
    else if(data == "") {
        nick = ""
        user = ""
        host = ""
        type = Prefix.PrefixType.None
    }
    // prefixes with only nick
    else  {
        nick = data
        user = " "
        host = " "
        type = Prefix.PrefixType.Nick
    }

    prefix = Prefix(nick,user,host,type)
    return prefix
}

fun isPrefix(data: String): Boolean {
    var foundExclamation = false  // !
    var foundAtTheRate = false // @
    for(i in data) {
        if(i == '!') foundExclamation = true
        else if(i == '@') foundAtTheRate = true
        else continue
    }
    return (foundExclamation && foundAtTheRate)
}

// data here is the data you want to compare
// prefix is the prefix from a channel

fun prefixMatchesString(data: String,prefix: Prefix): Boolean {
    var temp = data
    var found = false
    val nick = data.substring(0,data.indexOf('!'))
    temp = temp.substring(temp.indexOf("!") + 1, temp.length)
    val user = temp.substring(0,temp.indexOf("@"))
    val host = temp.substring(temp.indexOf("@") + 1, temp.length)

    // *!*@host
    if(nick == "*" && user == "*") {
        if(prefix.hostname == host)
            found = true
    }

    // *!user@*
    else if(nick == "*" && host == "*") {
        if(prefix.user == user)
            found = true
    }

    // nick!*@*
    else if(user == "*" && host == "*") {
        if(prefix.nick == nick)
            found = true
    }

    // nick!user@*
    else if(host == "*") {
        if(prefix.user == user && prefix.nick == nick)
            found = true
    }

    // *!user@host
    else if(nick == "*") {
        if(prefix.user == user && prefix.hostname == host)
            found = true
    }

    // nick!*@host
    else if(user == "*") {
        if(prefix.nick == nick && prefix.hostname == host)
            found = true
    }

    // nick!user@host
    else if(nick != "*" && user != "*" && host != "*") {
        if(prefix.nick == nick && prefix.hostname == host && prefix.user == user)
            found = true
    }

    else if(nick == "*" && user == "*" && host == "*") {
        if(prefix.nick == nick && prefix.hostname == host && prefix.user == user)
            found = true
    }

    else throw AghoraException("Error: At prefixMatchesString() in " +
            "com.github.otakusenpai.aghora.irc.ircmessage.Prefix: Unable to classify the prefix.")

    return found
}