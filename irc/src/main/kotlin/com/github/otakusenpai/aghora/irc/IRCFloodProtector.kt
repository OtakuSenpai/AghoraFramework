package com.github.otakusenpai.aghora.irc

import com.github.otakusenpai.aghora.commonUtils.log.getTimeStamp
import com.github.otakusenpai.aghora.commonUtils.log.log
import com.github.otakusenpai.aghora.commonUtils.log.logMessages
import com.github.otakusenpai.aghora.irc.connection.Connection
import kotlinx.coroutines.experimental.delay

class IRCFloodProtector() {

    constructor(connection: Connection,burstMax: Int,ifToBurst: Boolean) : this() {
        canBurst = ifToBurst
        burstLength = burstMax
        conn = connection
    }

    fun addMessage(target: String, data: String) {
        messageList.add(Pair(target,data))
    }

    /**
     * @param target Is the user to whom the message will be send
     * @param toFlush It is a boolean, represent whether to flush or not
     * @return Unit
     * @since 0.1.7.1
     * This method will return the messages to the target, with a option
     * to flush all the messages to the said target.
     **/
    suspend fun sendMsg(target: String,toFlush: Boolean) {
        if(messageList.isNotEmpty()) {
            val foo =
                    messageList.partition { it.first == target }
            messageList = foo.second as MutableList
            val tmpList = foo.first as MutableList
            log("${getTimeStamp()} MessageList size = ${messageList.size}", logMessages)
            for(msg in tmpList) {
                if(toFlush)
                    SendRawMsg(msg.second,conn)
                else {
                    delay(1000L)
                    SendRawMsg(msg.second, conn)
                }
            }
            tmpList.clear()
        }
    }

    /**
     * @param target Is the user to whom the message will be send
     * @return Unit
     * @since 0.1.7.1
     * This method will return the messages to the target, with
     * flushing of all the messages to the said target as set while constructing.
     */
    fun sendMsgWithFlush(target: String) {
        val foo =
                messageList.partition { it.first == target }
        messageList = foo.second as MutableList<Pair<String, String>>

        for(msg in foo.first) {
            SendRawMsg("$target ${msg.second}",conn)
        }
    }

    /**
     * @return Unit
     * @since 0.1.7.1
     * This function flushes all the messages in the list.
     * Applies the burst limit while flushing.
     */
    suspend fun flushAll() {

        if(messageList.size > 0) {
            var tmpList = mutableListOf<Pair<String,String>>()
            messageList.reverse()
            while(messageList.isNotEmpty()) {
                tmpList = messageList.subList(0, burstLength)
                for(i in 0 until burstLength) messageList.removeAt(i)

                if(canBurst)
                    tmpList.forEach { SendRawMsg(it.second,conn) }
                else {
                    delay(700L)
                    tmpList.forEach { SendRawMsg(it.second,conn) }
                }
            }
        }
    }

    suspend fun flushAll(target: String) {
        var tmp =
                messageList.partition { it.first == target }
        messageList = tmp.second as MutableList<Pair<String, String>>
        var todoList = tmp.first as MutableList
        todoList.reverse()
        var tmpList = mutableListOf<Pair<String,String>>()
        while(todoList.isNotEmpty()) {
            if(todoList.size < burstLength) {
                tmpList = todoList
            } else tmpList = todoList.subList(0, burstLength)
            for(i in tmpList) todoList.remove(i)

            if(canBurst)
                tmpList.forEach { SendRawMsg(it.second,conn) }
            else {
                delay(700L)
                tmpList.forEach { SendRawMsg(it.second,conn) }
            }
        }
    }

    private var canBurst = false
    private var burstLength = 5
    private lateinit var conn: Connection
    // First one is target
    // Second one is message
    open var messageList = mutableListOf<Pair<String,String>>()
}