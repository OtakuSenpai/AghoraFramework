package com.github.otakusenpai.aghora.irc.ircmessage.events


import com.github.otakusenpai.aghora.irc.MsgType
import com.github.otakusenpai.aghora.irc.PacketType
import com.github.otakusenpai.aghora.irc.ircmessage.*
import kotlin.text.*

// :Nawab!~OtakuSenp@unaffiliated/otakusenpai PRIVMSG #freenode :like this one

class PrivMsgEvent(value: MsgType) : Event(value) {
    override fun onNotify(data: String,msgtype: MsgType): MsgData {
        lateinit var msg: MsgData
        var content: String = ""
        lateinit var prefix: Prefix
        lateinit var command: String
        lateinit var sender: String
        lateinit var packet: PacketType
        try {
            if (this.msg == msgtype) {
                var dataList = data.split("\\s{1,}".toRegex())
                var contentList = dataList.subList(3,dataList.size)
                for(i in contentList)
                    content = content + " " + i

                var otherList = dataList.subList(0,3)

                prefix = retPrefix(otherList.get(0))
                command = otherList[1]
                sender = otherList[2]

                packet = PacketType.OTHER
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        }
        msg = MsgData(prefix, command, sender, content, packet)

        return msg
    }
}