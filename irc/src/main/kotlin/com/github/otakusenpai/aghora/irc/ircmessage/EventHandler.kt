package com.github.otakusenpai.aghora.irc.ircmessage

import com.github.otakusenpai.aghora.irc.MsgType

class EventHandler(message: MsgType) {
    fun subscribeToEvent(event: Event) {
        eventCallListeners.add(event)
    }

    fun fireEvents(data: String): MsgData {
        lateinit var msg: MsgData
        for(i in eventCallListeners) {
            msg = i.onNotify(data,msgtype)
        }
        return msg
    }

    var msgtype: MsgType = message
    internal var eventCallListeners: MutableList<Event> = mutableListOf<Event>()
}