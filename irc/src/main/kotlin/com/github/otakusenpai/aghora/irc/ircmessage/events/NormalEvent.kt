package com.github.otakusenpai.aghora.irc.ircmessage.events

import com.github.otakusenpai.aghora.commonUtils.error.AghoraException
import com.github.otakusenpai.aghora.commonUtils.isNumeric
import com.github.otakusenpai.aghora.commonUtils.log.log
import com.github.otakusenpai.aghora.commonUtils.log.logMessages
import com.github.otakusenpai.aghora.irc.MsgType
import com.github.otakusenpai.aghora.irc.PacketType
import com.github.otakusenpai.aghora.irc.ircmessage.*
import com.github.otakusenpai.aghora.irc.retPacketType


// :Nawab!~OtakuSenp@unaffiliated/otakusenpai PRIVMSG #freenode :like this on

class NormalEvent(value: MsgType) : Event(value) {
    override fun onNotify(data: String,msgtype: MsgType): MsgData {
        lateinit var msg: MsgData
        lateinit var content: String
        var prefix = Prefix()
        lateinit var command: String
        lateinit var sender: String
        lateinit var packet: PacketType

        log("Message is = $data", logMessages)
        try {
            if (this.msg == msgtype) {
                var dataList = data.split("\\s{1,}".toRegex())
                content = data.substring(data.indexOf(dataList.get(3)) + dataList[3].length, data.length)
                var others = data.substring(0,data.indexOf(dataList.get(3)) + dataList[3].length)

                var otherList = others.split("\\s{1,}".toRegex())
                if(isPrefix(otherList[0])) {
                    prefix = retPrefix(otherList.get(0))
                    command = otherList.get(1)
                    sender = otherList.get(2)

                    if (isNumeric(command)) {
                        packet = retPacketType(otherList[1].toInt())
                    } else packet = PacketType.OTHER
                } else {
                    prefix = Prefix("")
                    command = ""
                    sender = ""
                    packet = PacketType.OTHER
                }
            }
        } catch(e: Exception) {
            AghoraException("Data = $data",e)
        }
        msg = MsgData(prefix, command, sender, content, packet)

        return msg
    }
}