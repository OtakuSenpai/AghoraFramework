package com.github.otakusenpai.aghora.irc.ircmessage

import com.github.otakusenpai.aghora.commonUtils.hasIt
import com.github.otakusenpai.aghora.irc.MsgType
import com.github.otakusenpai.aghora.irc.PacketType
import com.github.otakusenpai.aghora.irc.addEvent
import com.github.otakusenpai.aghora.irc.findType

import kotlin.NullPointerException

class IRCMessage() {

    constructor(data: String): this() {
        // log("In ctor", logMessages)
        originalMsg = data
        // log("First", logMessages)
        var dataList = data.split("\\s{1,}".toRegex())
        // log("Second", logMessages)
        msgtype = findType(dataList.get(1))
        // log("Third", logMessages)
        if(hasIt(data,"PING"))
            msgtype = MsgType.ping
        // log("Fourth", logMessages)
        Parse()
        // log("Fifth", logMessages)
    }

    fun Parse() {
        var temp = originalMsg
        // log("Assign", logMessages)
        if(temp.elementAt(0) == ':')
            temp = temp.substring(1,temp.length)
        // log("if", logMessages)
        var eventHandler = EventHandler(msgtype)
        // log("eventHandler", logMessages)
        val event: Event? = addEvent(msgtype)
        // log("addEvent", logMessages)
        if(event is Event) {
            eventHandler.subscribeToEvent(event)
        }
        // log("subscribeToEvent", logMessages)
        msg = eventHandler.fireEvents(temp)
        // log("msg", logMessages)
    }

    fun printMsgData() {
        println("Prefix: ${msg.prefix.getData()}")
        println("Command: ${msg.command}")
        println("Sender: ${msg.sender}")
        println("Content: ${msg.content}")
        println("PacketType: ${msg.packet}")
        println("MsgType: ${msgtype}")
    }

    // RPL_WHOREPLY & RPL_ENDOFWHO are not returned in this function

    fun retMsgData() : String? {
        var ret: String? = null
        try {
            ret = when (msgtype) {
                MsgType.ping -> msg.command + " " + msg.content
                MsgType.join -> msg.prefix.getData() + " " + msg.command + " " + msg.content
                MsgType.normal -> msg.prefix.getData() + " " + msg.command +
                        " " + msg.sender + " " + msg.content
                MsgType.notice -> msg.prefix.getData() + " " + msg.command +
                        " " + msg.sender + " " + msg.content
                MsgType.mode -> msg.prefix.getData() + " " + msg.command +
                        " " + msg.sender + " " + msg.content
                MsgType.privmsg -> msg.prefix.getData() + " " + msg.command +
                        " " + msg.sender + " " + msg.content
                MsgType.special -> msg.prefix.getData() + " " + msg.command +
                        " " + msg.sender + " " + msg.content
                MsgType.topic -> msg.prefix.getData() + " " + msg.command +
                        "  " + msg.sender + " " + msg.content
                MsgType.namelist -> msg.prefix.getData() + " " + msg.command +
                        " " + msg.sender + " " + msg.content
                MsgType.quit -> msg.prefix.getData() + " " + msg.command +
                        " " + msg.content
                MsgType.topic_whotime -> msg.prefix.getData() + " " + msg.command +
                        " " + msg.sender + " " + msg.content
                MsgType.end_of_namelist -> this.originalMsg
                MsgType.host_hidden -> this.originalMsg
                MsgType.no_text_to_send -> msg.prefix.getData() + " " + msg.command +
                        " " + msg.sender + " " + msg.command
                MsgType.nick_name_in_use -> this.originalMsg
                else -> null
            }

            if (ret == null) {
                throw NullPointerException("IRCMessagge: Unknown message type....")
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        }

        return ret
    }

    fun clear() {
        msg.content = " "
        msg.sender = " "
        msg.command = " "
        msg.packet = PacketType.NONE
        msg.prefix.clear()
    }

    fun retPrefix() = msg.prefix
    fun retCommand() = msg.command
    fun retSender() = msg.sender
    fun retContent() = msg.content
    fun retPacket() = msg.packet

    lateinit var originalMsg: String
    private lateinit var msg: MsgData
    lateinit var msgtype: MsgType
}
