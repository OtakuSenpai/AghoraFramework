package com.github.otakusenpai.aghora.irc

import com.github.otakusenpai.aghora.irc.ircmessage.Event
import com.github.otakusenpai.aghora.irc.ircmessage.IRCMessage
import com.github.otakusenpai.aghora.irc.ircmessage.events.*

fun hasCommand(msg: IRCMessage, command: String): Boolean = msg.retCommand() == command

enum class PacketType(val packet: Int) {
    NONE(0),
    RPL_WELCOME(1),    // First four mean successful connection
    RPL_YOURHOST(2),
    RPL_CREATED(3),
    RPL_MYINFO(4),
    RPL_BOUNCE(5),   // Sent by the server to a user to suggest an alternative
    // server. This is often used when the connection is
    // refused because the server is already full.
    RPL_AWAY(301),           // "<nick> :<away message>"
    RPL_UNAWAY(305),         // ":You are no longer marked as being away"
    RPL_NOWAWAY(306),        // ":You have been marked as being away"
    RPL_WHOISUSER(311),      // "<nick> <user> <host> * :<real name>"
    RPL_WHOISSERVER(312),    // "<nick> <server> :<server info>"
    RPL_WHOISOPERATOR(313),  // "<nick> :is an IRC operator"
    RPL_WHOISCHANNELS(319),  // "<nick> :*( ( "@" / "+" ) <channel> " " )"
    RPL_ENDOFWHOIS(318),     // "<nick> :End of WHOIS list"
    RPL_WHOWASUSER(314),     // "<nick> <user> <host> * :<real name>"
    RPL_ENDOFWHOWAS(369),    // "<nick> :End of WHOWAS"
    RPL_LIST(322),           // "<channel> <# visible> :<topic>"
    RPL_LISTEND(323),        // ":End of LIST"
    RPL_NOTOPIC(331),        // "<channel> :No topic is set"
    RPL_TOPIC(332),          // "<channel> :<topic>"
    RPL_TOPICWHOTIME(333),
    RPL_INVITING(341),       // "<channel> <nick>"
    RPL_WHOREPLY(352),       // "<channel> <user> <host> <server> <nick>
    // ("H" / "G" > ["*"] [ ( "@" / "+" ) ]
    // :<hopcount> <real name>"
    RPL_ENDOFWHO(315),       // "<name> :End of WHO list"
    RPL_BANLIST(367),        // "<channel> <banmask>"
    RPL_ENDOFBANLIST(368),   // "<channel> :End of channel ban list"
    RPL_INFO(371),           // ":<string>"
    RPL_ENDOFINFO(374),      // ":End of INFO list"
    RPL_MOTDSTART(375),      // ":- <server> Message of the day - "
    RPL_ENDOFMOTD(376),      // ":End of MOTD command"
    RPL_MOTD(372),           // ":- <text>"
    RPL_YOUREOPER(381),      // ":You are now an IRC operator", send by
    // an OPER message
    RPL_TIME(391),           // "<server> :<string showing server's
    // local time>"
    RPL_UMODEIS(221),        // "<user mode string>"
    RPL_LUSERCLIENT(251),    // ":There are <integer> users and <integer>
    // services on <integer> servers"
    RPL_LUSEROP(252),        // "<integer> :operator(s) online"
    RPL_LUSERUNKNOWN(253),   // "<integer> :unknown connection(s)"
    RPL_LUSERCHANNELS(254),  // "<integer> :channels formed"
    RPL_LUSERME(255),        // ":I have <integer> clients and <integer>
    // servers"

    ERR_NOSUCHNICK(401),       // "<nickname> :No such nick/channel"
    ERR_NOSUCHSERVER(402),     // "<server name> :No such server"
    ERR_NOSUCHCHANNEL(403),    // "<channel name> :No such channel"
    ERR_CANNOTSENDTOCHAN(404), // "<channel name> :Cannot send to channel"
    ERR_TOOMANYCHANNELS(405),  // "<channel name> :You have joined too many channels"
    ERR_WASNOSUCHNICK(406),    // "<nickname> :There was no such nickname"
    ERR_TOOMANYTARGETS(407),   // "<target> :<com.otakusenpai.aghora.util.botUtils.com.github.otakusenpai.aghora.commonUtils.error code> recipients. <abort message>"
    ERR_NOORIGIN(409),         // ":No origin specified"
    // PING or PONG message missing the originator parameter.
    ERR_NORECIPIENT(411),      // ":No recipient given (<command>)"
    ERR_NOTEXTTOSEND(412),     // ":No text to send"
    ERR_NOTOPLEVEL(413),       // "<mask> :No toplevel domain specified"
    ERR_BADMASK(415),          // "<mask> :Bad Server/host mask"
    ERR_UNKNOWNCOMMAND(421),   // "<command> :Unknown command"
    ERR_NOMOTD(422),           //  ":MOTD File is missing"
    ERR_NOADMININFO(423),      // "<server> :No administrative info available"
    ERR_FILEERROR(424),        // ":File com.otakusenpai.aghora.util.botUtils.com.github.otakusenpai.aghora.commonUtils.error doing <file op> on <file>"
    // Generic com.otakusenpai.aghora.util.botUtils.com.github.otakusenpai.aghora.commonUtils.error message used to report a failed file
    // operation during the processing of a message.
    ERR_NONICKNAMEGIVEN(431),  // ":No nickname given"
    ERR_ERRONEUSNICKNAME(432), // "<nick> :Erroneous nickname"
    // Returned after receiving a NICK message which contains
    // characters which do not fall in the defined set.
    ERR_NICKNAMEINUSE(433),    // "<nick> :Nickname is already in use"
    ERR_NICKCOLLISION(436),    // "<nick> :Nickname collision KILL from <user>@<host>"
    // Returned by a server to a client when it detects a
    // nickname collision (registered of a NICK that
    // already exists by another server)
    ERR_UNAVAILRESOURCE(437),  // "<nick/channel> :Nick/channel is temporarily unavailable"
    // Returned by a server to a user trying to join a channel
    // currently blocked by the channel delay mechanism.
    // Returned by a server to a user trying to change nickname
    // when the desired nickname is blocked by the nick delay
    // mechanism.
    ERR_USERNOTINCHANNEL(441), // "<nick> <channel> :They aren't on that channel"
    ERR_NOTONCHANNEL(442),     // "<channel> :You're not on that channel"
    ERR_USERONCHANNEL(443),    // "<user> <channel> :is already on channel"
    ERR_NOLOGIN(444),          // "<user> :User not logged in"
    ERR_NOTREGISTERED(451),    // ":You have not registered"
    // Returned by the server to indicate that the client
    // MUST be registered before the server will allow it
    // to be parsed in detail.
    ERR_NEEDMOREPARAMS(461),   // "<command> :Not enough parameters"
    // Returned by the server by numerous commands to
    // indicate to the client that it didn't supply enough
    // parameters.
    ERR_ALREADYREGISTRED(462), // ":Unauthorized command (already registered)"
    // Returned by the server to any link which tries to
    // change part of the registered details (such as
    // password or user details from second USER message).
    ERR_NOPERMFORHOST(463),    // ":Your host isn't among the privileged"
    // Returned to a client which attempts to register with
    // a server which does not been setup to allow
    // connections from the host the attempted connection
    // is tried.
    ERR_PASSWDMISMATCH(464),   // ":Password incorrect"
    // Returned to indicate a failed attempt at registering
    // a connection for which a password was required and
    // was either not given or incorrect.
    ERR_YOUREBANNEDCREEP(465), // ":You are banned from this server"
    ERR_YOUWILLBEBANNED(466),  // Sent by a server to a user to inform that access to the
    // server will soon be denied.
    ERR_UMODEUNKNOWNFLAG(501), // ":Unknown MODE flag"
    ERR_USERSDONTMATCH(502),   // ":Cannot change mode for other users"
    ERR_KEYSET(467),           // "<channel> :Channel key already set"
    ERR_CHANNELISFULL(471),    // "<channel> :Cannot join channel (+l)"
    ERR_UNKNOWNMODE(472),      // "<char> :is unknown mode char to me for <channel>"
    ERR_INVITEONLYCHAN(473),   // "<channel> :Cannot join channel (+i)"
    ERR_BANNEDFROMCHAN(474),   // "<channel> :Cannot join channel (+b)"
    ERR_BADCHANNELKEY(475),    // "<channel> :Cannot join channel (+k)"
    ERR_BADCHANMASK(476),      // "<channel> :Bad Channel Mask"
    ERR_NOCHANMODES(477),      // "<channel> :Channel doesn't support modes"
    ERR_BANLISTFULL(478),      // "<channel> <char> :Channel list is full"
    ERR_NOPRIVILEGES(481),     // ":Permission Denied- You're not an IRC operator"
    // Any command requiring operator privileges to operate
    // MUST return this com.otakusenpai.aghora.util.botUtils.com.github.otakusenpai.aghora.commonUtils.error to indicate the attempt was
    // unsuccessful.
    ERR_CHANOPRIVSNEEDED(482), // "<channel> :You're not channel operator"
    // Any command requiring 'chanop' privileges (such as
    // MODE messages) MUST return this com.otakusenpai.aghora.util.botUtils.com.github.otakusenpai.aghora.commonUtils.error if the client
    // making the attempt is not a chanop on the specified
    // channel.
    ERR_RESTRICTED(484),       // ":Your connection is restricted!"
    // Sent by the server to a user upon connection to indicate
    // the restricted nature of the connection (user mode "+r").
    ERR_UNIQOPPRIVSNEEDED(485),// ":You're not the original channel operator"
    // Any MODE requiring "channel creator" privileges MUST
    // return this com.otakusenpai.aghora.util.botUtils.com.github.otakusenpai.aghora.commonUtils.error if the client making the attempt is not
    // a chanop on the specified channel.
    // My own research
    RPL_NAMREPLY(353), // :verne.freenode.net 353 G33kb0i = #tsukibot :G33kb0i @ChanServ OtakuSenpai
    RPL_ENDOFNAMES(366), // :weber.freenode.net 366 BamBaka ##llamas :End of /NAMES list.
    RPL_HOSTHIDDEN(396), // wolfe.freenode.net 396 BamBaka unaffiliated/neel/bot/bambaka :is now your hidden host (set by services.)
    OTHER(999);

    override fun toString() = this.packet.toString()

    // List of commands with no numeric replies:-
    // PING, JOIN, PRIVMSG
}

enum class MsgType(val type: Int) { // Ping message or other
    normal(1),     // If there is a irc numeral to it
    ping(2),     // PING :hitchcock.freenode.net
    join(3),     // :WiZ!jto@tolsun.oulu.fi JOIN #Twilight_zone

    // JOIN #foo,#bar fubar,foobar
    privmsg(4),  // PRIVMSG Angel :yes I'm receiving it !

    // :Angel!wings@irc.org PRIVMSG Wiz :Are you receiving this message ?
    // first is from user, second from server
    notice(5),

    // :WiZ!jto@tolsun.oulu.fi PART #playzone notice = 5,
    // :cherryh.freenode.net NOTICE * :*** Looking up your hostname...
    mode(6),     // :G33kb0i MODE G33kb0i :+i
    special(7),  // numerals on join of a network and are sent from server
    quit(8),
    namelist(9),
    topic(10),
    topic_whotime(11),
    who(12),
    end_of_who(13),
    end_of_namelist(14),
    host_hidden(15),
    no_text_to_send(16),
    nick_name_in_use(17),
    unknown_command(18);

    override fun toString() = this.type.toString()
};

fun findType(data: String) : MsgType {

    var type = when (data) {
        "PING" -> MsgType.ping
        "JOIN" -> MsgType.join
        "PRIVMSG" -> MsgType.privmsg
        "MODE" -> MsgType.mode
        "QUIT" -> MsgType.quit
        "NOTICE" -> MsgType.notice
        "353" -> MsgType.namelist
        "332" -> MsgType.topic
        "333" -> MsgType.topic_whotime
        "QUIT" -> MsgType.quit
        "352" -> MsgType.who
        "315" -> MsgType.end_of_who
        "366" -> MsgType.end_of_namelist
        "412" -> MsgType.no_text_to_send
        "421" -> MsgType.unknown_command
        "433" -> MsgType.nick_name_in_use
        "396" -> MsgType.host_hidden
        "001", "002", "004", "005", "252", "253", "255", "250", "266",
        "375", "376", "372", "265", "254", "251", "003" -> MsgType.special
        else -> MsgType.normal
    }
    return type
}

fun addEvent(value: MsgType): Event {
    var event = when (value) {
        MsgType.ping -> PingEvent(value)
        MsgType.join -> JoinEvent(value)
        MsgType.normal -> NormalEvent(value)
        MsgType.privmsg -> PrivMsgEvent(value)
        MsgType.mode -> ModeEvent(value)
        MsgType.notice -> NoticeEvent(value)
        MsgType.special -> SpecialEvent(value)
        MsgType.unknown_command -> UnknownCommandEvent(value)
        MsgType.namelist -> NameListEvent(value)
        MsgType.end_of_namelist -> EndOfNameListEvent(value)
        MsgType.topic -> TopicEvent(value)
        MsgType.no_text_to_send -> NoTextToSendEvent(value)
        MsgType.topic_whotime -> TopicWhoTimeEvent(value)
        MsgType.nick_name_in_use -> NickNameInUseEvent(value)
        MsgType.host_hidden -> HostHiddenEvent(value)
        MsgType.quit -> QuitEvent(value)
        MsgType.who -> WhoEvent(value)
        MsgType.end_of_who -> EndOfWhoEvent(value)
    }

    return event
}

/*
fun addEvent(value: MsgType): Event? {
    var event = if(value == MsgType.ping) {
        PingEvent(value)
    } else if(value == MsgType.join) {
        JoinEvent(value)
    }
    else null
    return event
}
*/

fun retPacketType(data: Int): PacketType {
    var packet = PacketType.values().map { it.packet to it }.toMap().get(data)
    if (packet == null) packet = PacketType.NONE
    return packet
}