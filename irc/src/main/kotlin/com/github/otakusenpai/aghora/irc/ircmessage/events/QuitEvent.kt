package com.github.otakusenpai.aghora.irc.ircmessage.events

import com.github.otakusenpai.aghora.irc.MsgType
import com.github.otakusenpai.aghora.irc.PacketType
import com.github.otakusenpai.aghora.irc.ircmessage.*

// :thiras!~thiras@unaffiliated/thiras QUIT :Ping timeout: 246 seconds
// :TheBetrayer!~Android@2607:fb90:178b:f7a4:b407:e555:2e9a:a505 QUIT :Max SendQ exceeded

class QuitEvent(value: MsgType) : Event(value) {
    override fun onNotify(data: String,msgtype: MsgType): MsgData {
        lateinit var msg: MsgData
        lateinit var content: String
        lateinit var prefix: Prefix
        lateinit var command: String
        lateinit var sender: String
        lateinit var packet: PacketType

        try {
            if (this.msg == msgtype) {
                content = data.substring(data.indexOf("QUIT")+5, data.length)
                var others = data.substring(0, data.indexOf("QUIT")+5)
                var otherList = others.split("\\s{1,}".toRegex())
                prefix = retPrefix(otherList.get(0))
                command = otherList.get(1)
                sender = ""

                packet = PacketType.OTHER
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        }
        msg = MsgData(prefix, command, sender, content, packet)

        return msg
    }
}