package com.github.otakusenpai.aghora.irc.connection

import io.ktor.network.sockets.Socket
import io.ktor.network.sockets.isClosed
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.io.ByteReadChannel
import kotlinx.coroutines.experimental.io.ByteWriteChannel
import kotlinx.coroutines.experimental.io.close

abstract class Connection() {

    abstract fun Connect()

    abstract suspend fun sendDataAsync(data: String)
    abstract fun sendData(data: String)
    abstract suspend fun receiveData(): String?
    abstract suspend fun receiveUTF8Data(): String?

    fun Disconnect() {
        output.close()
        socket.close()
        connected = false
    }

    open var connected: Boolean = false
    open lateinit var output : ByteWriteChannel
    open lateinit var input: ByteReadChannel
    open lateinit var socket: Socket
    open var port: Int = 6667
    open var address: String = ""
}
