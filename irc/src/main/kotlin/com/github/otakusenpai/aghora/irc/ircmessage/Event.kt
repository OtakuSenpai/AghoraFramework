package com.github.otakusenpai.aghora.irc.ircmessage

import com.github.otakusenpai.aghora.irc.MsgType

// :SleepyGuy!~Aztec03@unaffiliated/aztec03 NICK :Aztec03
// :asimov.freenode.net 433 * LinChanX :Nickname is already in use.
// !raw NS GHOST LinChanX`

abstract class Event(message: MsgType) {
    abstract fun onNotify(data: String, msgtype: MsgType) : MsgData

    open var msg: MsgType = message
}
