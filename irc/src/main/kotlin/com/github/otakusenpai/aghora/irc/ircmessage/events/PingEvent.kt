package com.github.otakusenpai.aghora.irc.ircmessage.events

import com.github.otakusenpai.aghora.irc.MsgType
import com.github.otakusenpai.aghora.irc.PacketType
import com.github.otakusenpai.aghora.irc.ircmessage.*

import kotlin.text.*

// PING :verne.freenode.net

class PingEvent(value: MsgType) : Event(value) {
    override fun onNotify(data: String,msgtype: MsgType): MsgData {
        lateinit var msg: MsgData
        lateinit var content: String
        lateinit var prefix: Prefix
        lateinit var command: String
        lateinit var sender: String
        lateinit var packet: PacketType

        try {
            if (this.msg == msgtype) {
                prefix = Prefix("", "", "",Prefix.PrefixType.None)
                sender = " "
                command = data.substring(0, data.indexOf(" "))
                content = data.substring(data.indexOf(":"), data.length)
                packet = PacketType.OTHER
            }
        } catch (e: Throwable) {
            e.printStackTrace()
        }
        msg = MsgData(prefix,command,sender,content,packet)

        return msg
    }
}