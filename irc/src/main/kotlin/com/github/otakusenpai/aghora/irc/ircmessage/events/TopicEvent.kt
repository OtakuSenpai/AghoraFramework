package com.github.otakusenpai.aghora.irc.ircmessage.events

import com.github.otakusenpai.aghora.irc.MsgType
import com.github.otakusenpai.aghora.irc.PacketType
import com.github.otakusenpai.aghora.irc.ircmessage.*
import com.github.otakusenpai.aghora.irc.retPacketType

// :cherryh.freenode.net 332 G33kb0i ##llamas :The official #1 off-topic

class TopicEvent(value: MsgType) : Event(value) {
    override fun onNotify(data: String,msgtype: MsgType): MsgData {
        lateinit var msg: MsgData
        lateinit var content: String
        lateinit var prefix: Prefix
        lateinit var command: String
        lateinit var sender: String
        lateinit var packet: PacketType

        try {
            if (this.msg == msgtype) {
                content = data.substring(data.indexOf("#"), data.length)
                var others = data.substring(0, data.indexOf("#"))

                var otherList = others.split("\\s{1,}".toRegex())
                prefix = retPrefix(otherList.get(0))
                command = otherList.get(1)
                sender = otherList.get(3)

                if (otherList[1] == "332") {
                    packet = retPacketType(otherList[1].toInt())
                } else packet = PacketType.OTHER
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        }
        msg = MsgData(prefix, command, sender, content, packet)

        return msg
    }
}