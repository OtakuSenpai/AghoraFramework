package com.github.otakusenpai.aghora.irc.ircmessage.events

import com.github.otakusenpai.aghora.commonUtils.hasIt
import com.github.otakusenpai.aghora.irc.MsgType
import com.github.otakusenpai.aghora.irc.PacketType
import com.github.otakusenpai.aghora.irc.ircmessage.*
import kotlin.text.*

// :kornbluth.freenode.net 353 G33kb0i = #tsukibot :G33kb0i dx_ob
// :barjavel.freenode.net 353 BamBaka * ##helloworlders :BamBaka +Gustavo6046
// :niven.freenode.net 353 BamBaka @ ##llamas :BamBaka kurahaupo_

class NameListEvent(value: MsgType) : Event(value) {
    override fun onNotify(data: String,msgtype: MsgType): MsgData {
        lateinit var msg: MsgData

        lateinit var content: String
        lateinit var prefix: Prefix
        lateinit var command: String
        lateinit var sender: String
        lateinit var packet: PacketType

        try {
            if (this.msg == msgtype) {
                if(hasIt(data,'=')) {
                    content = data.substring(data.indexOf("=") + 1, data.length)
                    var others = data.substring(0, data.indexOf(":"))
                    var otherList = others.split("\\s{1,}".toRegex())

                    prefix = retPrefix(otherList.get(0))
                    command = otherList.get(1)
                    sender = otherList.get(4)

                    packet = PacketType.RPL_NAMREPLY
                } else if(hasIt(data,'*')) {
                    content = data.substring(data.indexOf("*") + 1, data.length)
                    var others = data.substring(0, data.indexOf(":"))
                    var otherList = others.split("\\s{1,}".toRegex())

                    prefix = retPrefix(otherList.get(0))
                    command = otherList.get(1)
                    sender = otherList.get(4)

                    packet = PacketType.RPL_NAMREPLY
                } else if(hasIt(data,'@')) {
                    content = data.substring(data.indexOf("@") + 1, data.length)
                    var others = data.substring(0, data.indexOf(":"))
                    var otherList = others.split("\\s{1,}".toRegex())

                    prefix = retPrefix(otherList.get(0))
                    command = otherList.get(1)
                    sender = otherList.get(4)

                    packet = PacketType.RPL_NAMREPLY
                }
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        }
        msg = MsgData(prefix, command, sender, content, packet)

        return msg
    }
}