package com.github.otakusenpai.aghora

import com.github.otakusenpai.aghora.bot.bot.AbstractBot
import com.github.otakusenpai.aghora.bot.bot.IRCBot
import com.github.otakusenpai.aghora.bot.server.AghoraServer
import com.github.otakusenpai.aghora.commonUtils.beginsWith
import com.github.otakusenpai.aghora.commonUtils.hasIt
import com.github.otakusenpai.aghora.irc.SendPrivMsg
import com.github.otakusenpai.aghora.irc.ircmessage.IRCMessage
import kotlinx.coroutines.experimental.runBlocking

class TestBot1(configPath: String,serverName: String,
               botName: String) : IRCBot(configPath,serverName,botName) {

    override suspend fun customMsgHandlers(data: MutableList<IRCMessage>) {
        try {
            for(msg in data) {
                val tempContent = if(hasIt(msg.retContent(),':')) {
                    msg.retContent().substring(msg.retContent().indexOf(':')+1,
                            msg.retContent().length)
                } else {
                    msg.retContent()
                }
                if(beginsWith(tempContent,"mwaaa") || beginsWith(tempContent,"mwaa"))
                    SendPrivMsg(msg.retSender(),"mwaaa!!!",conn)
            }
        } catch(e: Throwable) {

        }
    }
}

/*
class TestBot2(configPath: String,serverName: String,
               botName: String) : IRCBot(configPath,serverName,botName) {

    override suspend fun customMsgHandlers(data: MutableList<IRCMessage>) {
        try {
            for(msg in data) {
                val tempContent = if(hasIt(msg.retContent(),':')) {
                    msg.retContent().substring(msg.retContent().indexOf(':')+1,
                            msg.retContent().length)
                } else {
                    msg.retContent()
                }
                if(beginsWith(tempContent,"hey") || beginsWith(tempContent,"hi"))
                    SendPrivMsg(msg.retSender(),"hey there!",conn)
            }
        } catch(e: Throwable) {

        }
    }
}
*/

fun main(args: Array<String>) = runBlocking {

    val configPath = "./bot.config"
    val server = AghoraServer(configPath,"./logs/server/")
    // println("Initialised")
    val test1 = TestBot1(configPath,"Freenode","ircbot")
    // val test2 = TestBot2(configPath, "Freenode", "TestBot")
    // println("Defined bot")
    server.addBot(test1 as AbstractBot,test1.botname)
    // server.addBot(test2 as AbstractBot, "TestBot")
    // println("Added bot")
    server.runBots()
    server.dispose()

    /*
    val msg = IRCMessage(":Nawab!~OtakuSenp@unaffiliated/neel PRIVMSG #AghoraBot :,join ##llamas")
    msg.printMsgData()
    */

}