package com.github.otakusenpai.aghora.commonUtils.botdata

data class BotDetails(val owner: String,val nick1: String,val nick2: String,val user: String,
                      val realname: String, val specialChar: Char,val port: Int,val address: String,
                      val useSSL: Boolean,val passwdUsed: Boolean, val password: String, val botCommand: List<String>)
