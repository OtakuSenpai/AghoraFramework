package com.github.otakusenpai.aghora.commonUtils.time

import java.time.LocalDateTime

val SECONDS_PER_MINUTE: Long = 60
val SECONDS_PER_HOUR = SECONDS_PER_MINUTE * 60
val SECONDS_PER_DAY = SECONDS_PER_HOUR * 24
val MINUTES_PER_DAY: Long = (SECONDS_PER_HOUR * 24) / 60

enum class AghoraTimeType(var type: Int) {
    TEST(0),
    UNBAN(1),
    UNBANWITHEXCEMPT(2);

    override fun toString(): String = this.type.toString()
}

// var sendUnban =  { channel: String, prefix: String -> String }

inline fun toLong(aghoraTime: AghoraTime): Long = aghoraTime.seconds + (aghoraTime.minutes * SECONDS_PER_MINUTE) + (aghoraTime.hours *
        SECONDS_PER_HOUR) + (aghoraTime.days * MINUTES_PER_DAY * SECONDS_PER_MINUTE)

inline fun isBetweenInclusive(candidate: LocalDateTime, start: LocalDateTime, end: LocalDateTime): Boolean = !candidate.isBefore(start) && !candidate.isAfter(end)  // Inclusive.

inline fun isBetweenExclusive(candidate: LocalDateTime, start: LocalDateTime, end: LocalDateTime): Boolean = candidate.isBefore(start) && candidate.isAfter(end);  // Exclusive of end.

fun addAghoraTime(now: AghoraTime, other: AghoraTime): AghoraTime {
    val nowLong = now.toLong()
    val otherLong = other.toLong()

    val finalValue = createAghoraTime(nowLong + otherLong)
    return finalValue
}

fun createAghoraTime(time: Long): AghoraTime {
    var timeRet = AghoraTime()
    timeRet.days = time / SECONDS_PER_DAY
    timeRet.hours = time / SECONDS_PER_HOUR - ( timeRet.days * 24 )
    timeRet.minutes = time  / SECONDS_PER_MINUTE - ( timeRet.hours * 60 +
            timeRet.days * MINUTES_PER_DAY)
    timeRet.seconds = time  - ( timeRet.hours * SECONDS_PER_HOUR +
            timeRet.days * SECONDS_PER_DAY + timeRet.minutes * SECONDS_PER_MINUTE)
    return timeRet
}

fun parseTimeInput(data: String): Triple<Int,String,Boolean> {
    var foundTimeStamp = false
    var numbersAreThere = false
    var numberStr = ""

    val charDataList = data.toMutableList()
    // Get the Time format - Seconds, Minutes, Hours or days
    val timeStamp = charDataList.get(charDataList.size - 1)
    // Get the numbers
    var numberList = charDataList.subList(0,charDataList.size - 1)

    // Check if numberList is a list of digits

    for(i in numberList) {
        if(i.isDigit()) numberStr = numberStr + i
        else {
            numbersAreThere = false
            break
        }
    }

    numbersAreThere = true

    if(timeStamp.isLetter())
        foundTimeStamp = true

    // Specify type
    var time = 0
    if(timeStamp == 's' || timeStamp == 'S')
        time = 1
    else if(timeStamp == 'm' || timeStamp == 'M')
        time = 2
    else if(timeStamp == 'h' || timeStamp == 'H')
        time = 3
    else if(timeStamp == 'd' || timeStamp == 'D')
        time = 4
    return Triple(time,numberStr,(numbersAreThere && foundTimeStamp))
}

fun setTimeInNumbers(time: String): Long {
    val data = parseTimeInput(time)

    val timeMultiplier = getTimeMultiplier(data.first)
    val timeInDetails = timeMultiplier * (data.second).toLong() * 1000
    return timeInDetails
}

fun getTimeMultiplier(timeNumber: Int): Int {
    var timeInDigits = 0

    if(timeNumber == 2)
        timeInDigits = 60
    else if(timeNumber == 3)
        timeInDigits = 60 * 60
    else if(timeNumber == 4)
        timeInDigits = (60 * 60) * 24
    else if(timeNumber == 1)
        timeInDigits = 1

    return timeInDigits
}