package com.github.otakusenpai.aghora.commonUtils.error

class AghoraException  : Exception {
    constructor(message: String) : super(message)
    constructor(message: String, e: Exception) :  super(message,e)
    constructor(message: String, e: Throwable) :  super(message,e)
}