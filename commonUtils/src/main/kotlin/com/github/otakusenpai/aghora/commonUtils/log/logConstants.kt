package com.github.otakusenpai.aghora.commonUtils.log

import java.time.DateTimeException
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

var logMessages = true

fun log(msg: String, logOrNot: Boolean) = when(logOrNot) {
    true  -> println(message = "[${Thread.currentThread().name}] $msg")
    false -> logMessages = logOrNot
}

fun log(botName: String, msg: String, logOrNot: Boolean) = when(logOrNot) {
    true  -> println(message = "[${Thread.currentThread().name}]: $botName = $msg")
    false -> logMessages = logOrNot
}

fun getThreadName(): String = "[${Thread.currentThread().name}]:"
fun getTimeStamp(): String {
    val zone = ZonedDateTime.now(ZoneId.systemDefault())
    val pattern = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).
            withLocale(Locale.getDefault())
    return zone.format(pattern) + ':'
}