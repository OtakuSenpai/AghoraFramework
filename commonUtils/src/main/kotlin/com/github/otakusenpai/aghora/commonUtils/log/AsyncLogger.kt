package com.github.otakusenpai.aghora.commonUtils.log

import com.github.otakusenpai.aghora.commonUtils.error.AghoraException
import kotlinx.coroutines.experimental.*
import sun.rmi.server.Dispatcher
import java.io.*
import java.nio.charset.Charset

class AsyncLogger : AbstractLogger {

    constructor(filepath: String,name: String) : super() {
        var file = File(filepath)
        file.mkdirs()
        filename = "$filepath/$name"
        fileOS = FileOutputStream(filename,true)
    }

    fun close() {
        fileOS.close()
        dataOS.close()

    }

   suspend fun log(toLog: Boolean,msg: String?) = coroutineScope {
       withContext(Dispatchers.IO) {
           if (toLog)
               writeToFile(msg!!)
       }
   }

   suspend fun log(toLog: Boolean,e: Throwable) = coroutineScope {
       withContext(Dispatchers.IO) {
           if (toLog)
               writeToFile(e?.message as String)
       }
   }

   suspend fun log(toLog: Boolean,toFile: Boolean,e: Throwable) = coroutineScope {
       withContext(Dispatchers.IO) {
           if(toLog)
               when(toFile) {
                   true -> writeToFile(e?.message as String)
                   false -> writeToFile(e?.message as String)
               }
       }
   }

   suspend fun log(toLog: Boolean,toFile: Boolean,msg: String?) = coroutineScope {
       withContext(Dispatchers.IO) {
           if(toLog)
               when(toFile) {
                   true -> writeToFile(msg!!)
                   false -> writeToFile(msg!!)
               }
       }
   }

   override fun writeToSTDOUT(msg: String) =
           println(message = "${getThreadName()} ${getTimeStamp()} $msg")

   override fun writeToFile(msg: String)  {
       dataOS = DataOutputStream(BufferedOutputStream(fileOS))
       val data = getThreadName() + " "  + getTimeStamp() + " " + msg + '\n'
       dataOS.write(data.toByteArray(Charsets.UTF_8))
       dataOS.flush()
   }

}