package com.github.otakusenpai.aghora.commonUtils.log

import java.io.DataOutputStream
import java.io.FileOutputStream

abstract class AbstractLogger {

    abstract fun writeToFile(msg: String)
    abstract fun writeToSTDOUT(msg: String)

    protected lateinit var filename: String
    protected lateinit var fileOS: FileOutputStream
    protected lateinit var dataOS: DataOutputStream
}