package com.github.otakusenpai.aghora.commonUtils.plugin

data class AghoraRetData(val message: String,val type: Int,val timeToAct: Long,
                         val command: String,val toActOrNot: Boolean)
