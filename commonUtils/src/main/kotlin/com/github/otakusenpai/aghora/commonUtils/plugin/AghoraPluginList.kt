package com.github.otakusenpai.aghora.commonUtils.plugin

object AghoraPluginsList {
    // K is plugin mastercommand(like for example, basic)
    // V is a list of subcommands(like for example, moo)

    var pluginMap = mutableMapOf<String,MutableList<String>>()
    var botCommands = mutableListOf<String>()

    fun subCommandIsThere(masterCommand: String,subCommand: String): Boolean {
        var found = false

        for((plugin,subCommands) in pluginMap) {
            if(plugin == masterCommand) {
                for(i in subCommands) {
                    if(i == subCommand)
                        found = true
                }
            }
        }
        return found
    }

    fun subCommandIsThere(subCommand: String): Boolean {
        var found = false

        for(i in pluginMap) {
            // println("MasterCommand is ${i.key}")
            for(subCommands in i.value) {
                // println("SubCommand is $subCommand to be matched with $subCommands")
                if(subCommand == subCommands) {
                    found = true
                }
            }
        }
        return found
    }

    // Used to return if a subCommand variable is present in the plugin list
    // Return type is a Pair of Boolean and String. String is the Mastercommand and
    // Boolean is determined by if its found or not

    fun retMasterCommand(subCommand: String): Pair<Boolean,String> {
        var found = false
        var mc = ""

        for(i in pluginMap) {
            for(subCommands in i.value) {
                println("SubCommand is $subCommand to be matched with $subCommands")
                if(subCommand == subCommands) {
                    mc = i.key
                    found = true
                }
            }
        }
        return Pair(found,mc)
    }

    // Checks if a string has a plugin command or not.
    // Used in BasicBot.kt
    // Return type is Boolean.

    fun hasPlgCommand(data: String): Boolean {
        var found = false
        val msgList: MutableList<String> = if(data.length > 2) { data.split("\\s{1,}".toRegex()) as
                MutableList<String> } else { mutableListOf() }
        val subCommand = if(msgList.isEmpty()) "" else msgList.get(0)
        // log("Subcommand is $subCommand", logMessages)
        if(subCommand == "") found = false
        else if(subCommandIsThere(subCommand.substring(1, subCommand.length))) {
            found = true
        }
        return found
    }

    // Return the master commands loaded from each plugin.
    // Return type is MutableList<String>

    fun getPluginMasterCommands(): MutableList<String> {
        var masterCommands = mutableListOf<String>()
        for(i in pluginMap) {
            masterCommands.add(i.key)
        }
        return masterCommands
    }

    // Return the total subCommands loaded from all plugins.
    // Return type is MutableList<String>

    fun getPluginSubCommands(): MutableList<String> {
        var sbCommand = mutableListOf<String>()
        for((ms,subCommands) in pluginMap) {
            for(subCommand in subCommands)
                sbCommand.add(subCommand)
        }
        return sbCommand
    }

    fun populate(masterCommand: String, subCommands: MutableList<String>) {
        println("MasterCommand Added: $masterCommand")
        var f = ""
        for(i in subCommands)
            f = f + " " + i
        println("SubCommands Added: $f")
        pluginMap[masterCommand] = subCommands
    }

    fun removePlgMapItem(masterCommand: String) {
        pluginMap.remove(masterCommand)
    }
}

enum class PlgCommand(val subCommand: Int) {
    none(-1),
    moo(0), //Does need a irc command callback
    source(1), //Does need a irc command callback
    list(2), //Does need a irc command callback
    desc(3), //Does need a irc command callback
    setMask(4), //Does need a irc command callback
    setChannel(5), //Does need a irc command callback
    kick(6), //Does need a irc command callback
    invite(7), //Does need a irc command callback
    limit(8), //Does need a irc command callback
    ban(9); // Needs One

    override fun toString(): String = this.subCommand.toString()
}