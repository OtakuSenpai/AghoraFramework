package com.github.otakusenpai.aghora.commonUtils.time

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.temporal.ChronoUnit

class AghoraTime() {

    constructor(seconds: Long, minutes: Long, hours: Long, days: Long) : this(){
        this.seconds = seconds
        this.minutes = minutes
        this.hours = hours
        this.days = days
    }

    constructor(aghoraTime: AghoraTime) : this() {
        this.seconds = aghoraTime.seconds
        this.minutes = aghoraTime.minutes
        this.hours = aghoraTime.hours
        this.days = aghoraTime.days
    }

    constructor(timeInSecs: Long) : this() {
        this.seconds = createAghoraTime(timeInSecs).seconds
        this.minutes = createAghoraTime(timeInSecs).minutes
        this.hours = createAghoraTime(timeInSecs).hours
        this.days = createAghoraTime(timeInSecs).days
    }

    inline fun clear() {
        seconds = 0
        minutes = 0
        hours = 0
        days = 0
    }

    inline fun toLong(): Long = this.seconds + (this.minutes * SECONDS_PER_MINUTE) + (this.hours *
            SECONDS_PER_HOUR) + (this.days * MINUTES_PER_DAY * SECONDS_PER_MINUTE)

    inline fun toLocalDateTime(): LocalDateTime = LocalDateTime.ofInstant(Instant.now()
            .plus(this.days,ChronoUnit.DAYS).plus(this.hours,ChronoUnit.HOURS)
            .plus(this.minutes,ChronoUnit.MINUTES).plusSeconds(this.seconds), ZoneId.systemDefault())

    fun set(aghoraTime: AghoraTime) {
        this.seconds = aghoraTime.seconds
        this.minutes = aghoraTime.minutes
        this.hours = aghoraTime.hours
        this.days = aghoraTime.days
    }

    fun set(timeInSecs: Long) {
        this.set(createAghoraTime(timeInSecs))
    }

    fun add(aghoraTime: AghoraTime) {
        val thisLong = this.toLong()
        val aghoraTimeLong = aghoraTime.toLong()

        val finalValue = createAghoraTime(thisLong + aghoraTimeLong)
        this.days = finalValue.days
        this.hours = finalValue.hours
        this.minutes = finalValue.minutes
        this.seconds = finalValue.seconds
    }

    fun add(timeInSecs: Long) {
        val thisLong = this.toLong()

        val finalValue = createAghoraTime(thisLong + timeInSecs)
        this.days = finalValue.days
        this.hours = finalValue.hours
        this.minutes = finalValue.minutes
        this.seconds = finalValue.seconds
    }

    override inline fun toString() = String.format("%05d Days %02d:%02d:%02d",days,hours,minutes,seconds)

    var seconds = 0L
    var minutes = 0L
    var hours = 0L
    var days = 0L
}
