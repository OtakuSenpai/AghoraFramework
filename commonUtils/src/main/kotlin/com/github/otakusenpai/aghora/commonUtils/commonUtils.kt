package com.github.otakusenpai.aghora.commonUtils

import java.util.*

inline fun getVersion(): String = "0.1.7.1"

fun hasIt(data: String,key: Char): Boolean {
    var found = false
    for(i in data) {
        if(i == key) {
            found = true
            break
        }
    }
    return found
}

fun hasIt(data: String?, key: String): Boolean {
    var found = false
    if(data != null) {
        var dataList =  data.split("\\s{1,}".toRegex())

        for(i in dataList) {
            if(i == key) {
                found = true
                break
            }
        }
    } else throw Exception("Constants.kt: Can't check null strings!")
    return found
}

fun hasIt(data: MutableList<String>,key: String): Boolean {
    var found = false
    for (i in data)
        if (i == key)
            found = true
    return found
}

fun isNumeric(input: String): Boolean = try {
    input.toInt()
    true
} catch(e: NumberFormatException) {
    false
}

enum class States(val state: Int) {
    notWorking(1),
    Connect(2),
    Nick(3),
    User(4),
    Auth(5),
    Working(6),
}

fun parseNumeral(data: String,key: String,delimiter: Char): Boolean {
    var found  = false
    lateinit var temp1: String
    // Get all the text before contents
    var others = data.substring(0,data.indexOf(delimiter))

    // Weed out Prefix
    others = others.substring(others.indexOf(" "),others.length)

    // CHange into a List<String> without considering the spaces inside
    var otherList =  others.split("\\s{1,}".toRegex())
    temp1 = otherList[1]
    if(temp1 == key) found = true
    return found
}

fun beginsWith(data: String, key: String): Boolean {
    var found  = false
    var msgList =  data.split("\\s{1,}".toRegex())
    if(msgList.get(0) == key) found = true
    else false
    return found
}

//fun moveMessages(original: MutableList<IRCMessage>, temporary: MutableList<IRCMessage>) {

//}

fun ClosedRange<Int>.random() =
        Random().nextInt(endInclusive - start) +  start
