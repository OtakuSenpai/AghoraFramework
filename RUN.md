# How to build and run

In this, we will discuss how to build and run the bot from scratch. Instructions are for windows and unix only.

## Building the bot

First of all download the bot's source from git. For windows users, [here](https://stackoverflow.com/questions/110205/want-to-download-a-git-repository-what-do-i-need-windows-machine) is a useful link.

### Windows

   [Comin Soon]

### Unix

To build the whole project, type this from your command line on the directory you downloaded the project in:
```
 ./gradlew build
```
To make a gradle wrapper type this in your terminal:
```
gradle wrapper
```
To build the project's jar files type this:
```
./gradlew build
./gradlew fatJar
```
To build a plugin, say BasicOps from scratch type this:
```
./gradlew :plugins:BasicOps:build
```
Possible plugin's names include BasicOps and ChanOps.

Then, after this copy all your files to the folder [run](./run/)(here we only show the command for BasicOps):
```
mv build/libs/AghoraBot-*.jar run/
mv build/plugin/BasicOps-*-jar run/plugin/
```

Included in the [run](./run/) folder is an example plugin config for BasicOps plugin. Also change your bot config to inclue the necessary parameters.
