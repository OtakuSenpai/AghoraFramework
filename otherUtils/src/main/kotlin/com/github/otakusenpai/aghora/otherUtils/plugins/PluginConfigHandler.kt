package com.github.otakusenpai.aghora.otherUtils.plugins

import nu.xom.*
import java.io.File
import java.io.FileOutputStream

class PluginConfigHandler {

    constructor(path: String) {
        pathToXml = path
        var xmlBuilder = Builder()
        var f = File(path)
        xmlDocument = xmlBuilder.build(f)
        rootElement = xmlDocument.rootElement
    }

    fun getName(): String {
        lateinit var name: String
        try {
            var name1 = rootElement.getFirstChildElement("name")
            name = name1.value
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return name
    }

    fun getDescription(): String? {
        lateinit var desc: String
        try {
            var desc1 = rootElement.getFirstChildElement("description")
            desc = desc1.value
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return desc
    }

    fun getMasterCommand(): String? {
        lateinit var msc: String
        try {
            var msc1 = rootElement.getFirstChildElement("MasterCommand")
            msc = msc1.value
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return msc
    }

    fun getChildCommands(): MutableList<String> {
        var childList: MutableList<String> = mutableListOf()

        try {
            var childrenList = rootElement.
                    getFirstChildElement("ChildCommands").childElements
            for(i in 0..(childrenList.size()-1)) {
                childList.add(childrenList.get(i).value)
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return childList
    }

    fun checkForChildCommand(name: String):Boolean {
        var found = false

        try {
            var childrenList = rootElement.
                    getFirstChildElement("ChildCommands").childElements
            for(i in 0..(childrenList.size()-1)) {
                if(childrenList.get(i).value == name){
                    found = true
                }
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return found
    }

    fun getPositiveChannelList(server: String): MutableList<String> {
        var channelList: MutableList<String> = mutableListOf()

        try {
            var childrenList =
                    rootElement.getFirstChildElement("networks").childElements
            for(i in 0..(childrenList.size()-1)) {
                if(childrenList.get(i).getAttributeValue("name") == server) {
                    val chanList = childrenList.get(i).childElements
                    for(i in 0..(chanList.size()-1))
                        if(chanList.get(i).getAttributeValue("allowed") == "yes")
                            channelList.add(chanList.get(i).getAttributeValue("name"))

                }
            }

        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }

        return channelList
    }

    // True = yes
    // False = no

    fun setChannelAttributes(condition: Boolean,chan: String,server: String) {
        try {
            var childrenList = rootElement.getFirstChildElement("networks")
                    .childElements
            for(i in 0..(childrenList.size()-1)) {
                if(childrenList.get(i).getAttributeValue("name") == server) {
                    val chanList = childrenList.get(i).childElements
                    for(i in 0..(chanList.size()-1)) {
                        if (chanList.get(i).getAttribute("name").value == chan) {
                            if (condition) {
                                chanList.get(i).getAttribute("allowed").value = "yes"
                            }
                            else {
                                chanList.get(i).getAttribute("allowed").value = "no"
                            }

                        }
                    }
                }
            }
            writeDoc(xmlDocument)
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
    }

    fun ifMasksNotAllowed(server: String,channel: String,prefix: String,plugin: String): Boolean {
        var found = false
        try {
            var childrenList = rootElement.
                    getFirstChildElement("networks").childElements
            for(i in 0..(childrenList.size()-1)) {
                if (childrenList.get(i).getAttributeValue("name") == server) {
                    val chanList = childrenList.get(i).childElements
                    for (i in 0..(chanList.size() - 1)) {
                        if (chanList.get(i).getAttributeValue("name") == channel &&
                                chanList.get(i).getAttributeValue("allowed") == "yes") {
                            val prefixList = chanList.get(i).childElements
                            for (i in 0..(prefixList.size() - 1)) {
                                if (prefixList.get(i).getAttributeValue("ident") == prefix) {
                                    val pluginList = prefixList.get(i).childElements
                                    for (i in 0..(pluginList.size() - 1)) {
                                        if(pluginList.get(i).getAttributeValue("name") == plugin) {
                                            if(pluginList.get(i).value == "yes") {
                                                found = true
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return found
    }

    fun setMasksAttributes(value: Boolean,server: String,channel: String,prefix: String,plugin: String) {
        try {
            val childrenList = rootElement.
                    getFirstChildElement("networks").childElements

            var foundServer = false
            var foundChannel = false
            var foundPrefix = false

            var e = Element("plugin")
            e.addAttribute(Attribute("name",plugin))
            for(i in 0..(childrenList.size()-1)) {
                if(childrenList.get(i).getAttributeValue("name") == server) {
                    foundServer = true
                    println("Found server")
                    val chanList = childrenList.get(i).childElements
                    for(i in 0..(chanList.size()-1)) {
                        if(chanList.get(i).getAttributeValue("name") == channel) {
                            foundChannel = true
                            println("Found channel")
                            val prefixList = chanList.get(i).childElements
                            for(i in 0..(prefixList.size()-1)) {
                                if(prefixList.get(i).getAttributeValue("ident") == prefix) {
                                    foundPrefix = true
                                    println("Found prefix")
                                    val pluginList = prefixList.get(i).childElements
                                    val plugins = prefixList.get(i)
                                    for(i in 0..(pluginList.size()-1)) {
                                        if(pluginList.get(i).getAttributeValue("name") == plugin) {
                                            println("Found plugin?")
                                            plugins.removeChild(pluginList.get(i))
                                            if(value)
                                                e.appendChild("yes")
                                            else
                                                e.appendChild("no")
                                            plugins.appendChild(e)
                                        }
                                    }
                                }
                            }
                            if(!foundPrefix) {
                                var Prefix = Element("prefix")
                                Prefix.addAttribute(Attribute("ident",prefix))
                                if(value)
                                    e.appendChild("yes")
                                else
                                    e.appendChild("no")
                                Prefix.appendChild(e)
                                chanList.get(i).appendChild(Prefix)
                            }
                        }
                    }
                    if(!foundChannel) {
                        var Channel = Element("channel")
                        Channel.addAttribute(Attribute("name",channel))
                        Channel.addAttribute(Attribute("allowed","yes"))
                        var Prefix = Element("prefix")
                        Prefix.addAttribute(Attribute("ident",prefix))
                        if(value)
                            e.appendChild("yes")
                        else
                            e.appendChild("no")
                        Prefix.appendChild(e)
                        Channel.appendChild(Prefix)
                        childrenList.get(i).appendChild(Channel)
                    }
                }
            }
            if(!foundServer) {
                var Server = Element("network")
                Server.addAttribute(Attribute("name",server))
                var Channel = Element("channel")
                Channel.addAttribute(Attribute("name",channel))
                Channel.addAttribute(Attribute("allowed","yes"))
                var Prefix = Element("prefix")
                Prefix.addAttribute(Attribute("ident",prefix))
                if(value)
                    e.appendChild("yes")
                else
                    e.appendChild("no")
                Prefix.appendChild(e)
                Channel.appendChild(Prefix)
                Server.appendChild(Channel)
                rootElement.getFirstChildElement("networks").appendChild(Server)
            }
            writeDoc(xmlDocument)
            reloadConfig()
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
    }

    fun writeDoc(doc: Document) {
        try {
            var f = FileOutputStream(pathToXml)
            var serializer = Serializer(f,"UTF-8")
            serializer.indent = 4
            serializer.maxLength = 80
            serializer.write(xmlDocument)
            reloadConfig()
        } catch(e: Throwable) {
            e.printStackTrace()
        }
    }

    fun reloadConfig() {
        var xmlBuilder = Builder()
        var f = File(pathToXml)
        xmlDocument = xmlBuilder.build(f)
        rootElement = xmlDocument.rootElement
    }

    var xmlDocument: Document
    var rootElement: Element
    var pathToXml: String = ""
}