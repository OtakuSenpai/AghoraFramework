# History

## 0.1.x

v0.1.0)      Created the basic bot structure, including message handling part

v0.1.1)      Created a plugin functionality

v0.1.2)      Created a plugin config loader class

v0.1.3)      Created a bot config loader class

v0.1.4)      Created a EndOfWho and RPL_WHOREPLY event.Changed the way how the bot handles channel joins.Changed the way how the "set" command works.

v0.1.4.1)    Changed MODE event parsing.

v0.1.4.2)    Made the main class smaller.

v0.1.5.1)    Fixing some bugs in BasicOps plugin.

v0.1.5.2)    Fixed message parsing of PRIVMSG event.

v0.1.5.3)    Made two new commands("source" and "version"). Changed the way "list" command works.  

v0.1.6.0)    Fixed channel nick update issue. Made changes to PluginConfigHandler and fixed Basic Group's moo,ping and setMask plugins.

v0.1.6.2)    Changed the return type of AghoraInterface, changing it to a data class. Added the ChanOps plugin with fixed KICK and INVITE functions. Implemented KICK and INVITE IRC commands.

### v0.1.7.0

1) Fixed faulty setPlugin function in BotFunction.kt.
2) Added a custom exception type called AghoraException.
3) Added a all purpose plugins and subcommand name handler called AghoraPluginList.
4) Added the callback feature in files IRCCallBack.kt. Need further implementation.
5) Changed the version info of jars in plugins BasicOps and ChanOps in gradle files.
6) Moved "source","version" and "list" to BasicOps. Added new plugin "whichPlugin" which tells which tells the plugin mastercommand associated with a subcommand.
7) Changed function pluginCall in BotFunctions.kt to handle bot plugin calls.

### v0.1.7.1

1) Moved all the stuff to new domain.
2) Implemented the server and abstract bot classes. This classes will help in running multiple bots of different protocols. Made a IRCBot base class which can be inherited to create custom bots. 
3) Implemented a flood protection system. Needs further improving. 
4) Bumped the Kotlin coroutines version to "0.30.1" and Ktor version to "0.9.5".
5) Fixed a error with hasPlgCommand()
6) Added 5 new event types - EndOfNameListEvent, HostHidden events NickNameInUseEvent, NoTextToSendEvent and UnknownCommandEvent.
7) Implemented a bot's logger class(not message logger) which logs thread name and time with messages asynchronously.
8) Changed the bot's config to add multiple bots. Implemented functions to handle the new config structure.
9) Various changes and fixes.