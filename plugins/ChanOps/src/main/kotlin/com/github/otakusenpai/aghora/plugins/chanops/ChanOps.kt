package com.github.otakusenpai.aghora.plugins.chanops

import com.github.otakusenpai.aghora.commonUtils.hasIt
import com.github.otakusenpai.aghora.commonUtils.isNumeric
import com.github.otakusenpai.aghora.commonUtils.plugin.AghoraRetData
import com.github.otakusenpai.aghora.commonUtils.time.parseTimeInput
import com.github.otakusenpai.aghora.commonUtils.time.setTimeInNumbers
import com.github.otakusenpai.aghora.irc.*
import com.github.otakusenpai.aghora.irc.ircmessage.IRCMessage
import com.github.otakusenpai.aghora.irc.ircmessage.isPrefix
import com.github.otakusenpai.aghora.otherUtils.plugins.PluginConfigHandler
import com.github.otakusenpai.aghora.pluginUtils.IRCBotData
import com.github.otakusenpai.aghora.pluginUtils.api.AghoraInterface

import kotlinx.coroutines.experimental.*

class ChanOps : AghoraInterface() {

    override fun init(path: String, botdata: IRCBotData) {
        try {
            this.botdata = botdata
            pluginConfig = PluginConfigHandler(path)
            configPath = path
            subCommands = pluginConfig.getChildCommands()
            masterCommand = pluginConfig.getMasterCommand() as String
            description = pluginConfig.getDescription() as String
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    // ,chan kick ##llamas Nawab
    // ,chan kick ##llamas Nawab message here on from now....
    // ,chan invite nick #channel
    // .chan limit #channel 10
    // .chan ban #channel prefix
    // .chan ban #channel prefix except

    override fun onCommand(data: IRCMessage): AghoraRetData {
        var retData = ""
        var type = 0
        var timeToAct = 0L
        var command = "none"
        var toAct = false

        println("data = $data")

        val one = launch(CommonPool) {
            try {
                val tempContent = if(hasIt(data.retContent(),':')) {
                    data.retContent().substring(data.retContent().indexOf(':')+1,
                            data.retContent().length)
                } else {
                    data.retContent()
                }
                val strList: MutableList<String> = tempContent.split("\\s{1,}".
                        toRegex()) as MutableList<String>

                val subCommand = strList.get(0).substring(1,strList.get(0).length)
                println("data at 0 = ${strList.get(0)}")
                val size = strList.size
                val i = 0

                if(pluginList.subCommandIsThere(subCommand) && pluginConfig.checkForChildCommand(strList.get(0))) {
                    if(subCommand == "kick") {
                        if(size <= 2) {
                            retData = "Usage: ${botdata.botDetails.specialChar}chan kick #channel nick OR " +
                                    "${botdata.botDetails.specialChar}chan kick #channel nick message"
                            type = 1
                        } else
                            for(i in botdata.chanList) {
                                if(i.retName() == strList.get(1)) {
                                    if(i.userInChannel(strList.get(2))) {
                                        if(size == 3) {
                                            retData = SendKick(strList.get(1), strList.get(2))
                                            type = 2
                                        } else if(size > 3) {
                                            val tmpList = strList.subList(3,strList.size)
                                            var msg = ""
                                            var first = true
                                            for(k in tmpList) {
                                                if(first) {
                                                    msg = msg + k + " "
                                                    first = false
                                                } else {
                                                    msg = msg + " " + k
                                                }
                                            }
                                            retData = SendKick(strList.get(1), strList.get(2), msg)
                                            type = 2
                                        } else {
                                            retData = "Usage: ${botdata.botDetails.specialChar}chan kick #channel nick OR" +
                                                    "${botdata.botDetails.specialChar}chan kick #channel nick message"
                                            type = 1
                                        }
                                    } else {
                                        retData = "User ${strList.get(2)} not found in channel ${i.retName()}"
                                        type = 1
                                    }
                                }
                            }
                    }

                    else if(subCommand == "invite") {
                        var found = false

                        if(size <= 2) {
                            retData = "Usage: ${botdata.botDetails.specialChar}chan invite #channel nick"
                            type = 1
                        }
                        for(i in botdata.chanList) {
                            if(i.retName() == strList.get(1)) {
                                retData = SendInvite(strList.get(1), strList.get(2))
                                type = 2
                                found = true
                            }
                        }

                        if(!found) {
                            retData = "I'm not present in channel ${strList.get(1)}"
                            type = 1
                        }

                    }

                    else if(subCommand == "limit") {
                        var found = false
                        if(size <= 2) {
                            retData = "Usage: ${botdata.botDetails.specialChar}chan limit #channel 10"
                            type = 1
                        } else if(size == 3) {
                            println("Channel from msg: ${strList.get(1)}")
                            for(i in botdata.chanList) {
                                println("Channel in list: ${i.retName()}")
                                if(i.retName() == strList.get(1)) {
                                    found = true
                                    if(isNumeric(strList.get(2))) {
                                        retData = SendChannelLimit(strList.get(1), strList.get(2).toInt())
                                        type = 2
                                    }
                                }
                            }
                        } else if(!found) {
                            retData = "I'm not present in channel ${strList.get(1)}"
                            type = 1
                        }
                    }

                    else if(subCommand == "ban") {
                        var found = false
                        if (size <= 2) {
                            retData = "Usage: ${botdata.botDetails.specialChar}chan ban #channel prefix or " +
                                    "${botdata.botDetails.specialChar}chan ban #channel prefix except or " +
                                    "${botdata.botDetails.specialChar}chan ban #channel prefix time or " +
                                    "${botdata.botDetails.specialChar}chan ban #channel prefix except time." +
                                    " Time can be in the following format: 6D or 6d(day), 10M or 10m(minute)." +
                                    " Following are some common literals for time: Second - s/S, Minute - m/M, Hour - h/H, Day - d,D. " +
                                    "Others need not be implemented."
                            type = 1
                        } else if (size > 3) {
                            for (i in botdata.chanList) {
                                if (i.retName() == strList.get(1)) {
                                    if (size == 3) { // 1st
                                        if (isPrefix(strList.get(2))) {
                                            retData = SendBan(strList.get(1), strList.get(2))
                                            type = 2
                                        }
                                    } else if (size == 4) { // 2nd n 3rd
                                        if (isPrefix(strList.get(2))) {
                                            if (isPrefix(strList.get(3))) { // 2nd
                                                retData = SendBanwithExcept(strList.get(1), strList.get(2), strList.get(3))
                                                type = 2
                                            } else if (parseTimeInput(strList.get(3)).third) { // 3rd
                                                val data = parseTimeInput(strList.get(3))
                                                timeToAct = setTimeInNumbers(data.second)
                                                retData = SendBan(strList.get(1), strList.get(2))
                                                type = 2
                                                command = "sendUnban"
                                                toAct = true
                                            }
                                        }
                                    } else if (size == 6) {
                                        if (isPrefix(strList.get(2)) && isPrefix(strList.get(3)) &&
                                                parseTimeInput(strList.get(4)).third) {
                                            val data = parseTimeInput(strList.get(4))
                                            timeToAct = setTimeInNumbers(data.second)
                                            retData = SendBanwithExcept(strList.get(1), strList.get(2), strList.get(3))
                                            command = "SendUnbanWithExcempt"
                                            toAct = true
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            } catch (e: Throwable) {
                e.printStackTrace()
            }
        }

        runBlocking { one.join() }

        val dataToRet = AghoraRetData(retData,type,timeToAct,command,toAct)
        return dataToRet
    }

    lateinit var configPath: String

}