package com.github.otakusenpai.aghora.plugins.basicops

import com.github.otakusenpai.aghora.commonUtils.getVersion
import com.github.otakusenpai.aghora.commonUtils.hasIt
import com.github.otakusenpai.aghora.commonUtils.plugin.AghoraPluginsList
import com.github.otakusenpai.aghora.commonUtils.plugin.AghoraRetData
import com.github.otakusenpai.aghora.commonUtils.random
import com.github.otakusenpai.aghora.irc.IRCCommands
import com.github.otakusenpai.aghora.irc.ircmessage.IRCMessage
import com.github.otakusenpai.aghora.otherUtils.plugins.PluginConfigHandler
import com.github.otakusenpai.aghora.pluginUtils.IRCBotData
import com.github.otakusenpai.aghora.pluginUtils.api.AghoraInterface

class BasicOps: AghoraInterface() {

    override fun init(path: String, botData: IRCBotData) {
        try {
            botdata = botData
            pluginConfig = PluginConfigHandler(path)
            configPath = path
            subCommands = pluginConfig.getChildCommands()
            masterCommand = pluginConfig.getMasterCommand() as String
            description = pluginConfig.getDescription() as String
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    // Data will be like this
    // ,moo
    // ,list
    // ,desc moo
    // ,remind letty tell hey there!
    // ,setMask ....
    // ,setChannel ##llamas allowed

    // data is a IRCMessage data, aka a normal ircMessage

    override fun onCommand(data: IRCMessage): AghoraRetData {
        lateinit var retData: String
        var type = 1
        var command = IRCCommands.SendPrivMsg.toString()
        var timeToAct = 0L
        var toActOrNot = false
        try {
            val tempContent = if(hasIt(data.retContent(),':')) {
                data.retContent().substring(data.retContent().indexOf(':')+1,
                        data.retContent().length)
            } else {
                data.retContent()
            }
            val strList: MutableList<String> = tempContent.split("\\s{1,}".
                    toRegex()) as MutableList<String>

            val subCommand = strList[0].substring(1,strList[0].length)
            val size = strList.size
            val i = 0
            if(pluginList.subCommandIsThere(subCommand) && pluginConfig.checkForChildCommand(subCommand)) {

                // ,moo
                if(subCommand == "moo") {
                    retData = when((0..3).random()) {
                        0 -> "\u000309,01m\u000302,07o\u000305,13o\u000310,06o\u000307,14!!!"
                        1 -> "\u000305,13m\u000309,01o\u000310,06o\u000311,13o\u000307,14!!!"
                        2 -> "\u000306,08m\u000302,05o\u000307,09o\u000305,13o\u000307,14!!!"
                        else -> "\u000306,08m\u000302,05o\u000307,09o\u000305,13o\u000307,14!!!"
                    }
                    type = 1
                    command = IRCCommands.SendPrivMsg.toString()
                    timeToAct = 0L
                }

                else if(subCommand == "source") {
                    retData = "The bot is hosted at https://github.com/OtakuSenpai/AghoraBot"
                    type = 1
                    command = IRCCommands.SendPrivMsg.toString()
                    timeToAct = 0L
                }

                else if(subCommand == "version") {
                    retData = "My version is ${getVersion()}"
                    type = 1
                    command = IRCCommands.SendPrivMsg.toString()
                    timeToAct = 0L
                }

                else if(subCommand == "whichPlugin") {
                    if(strList.size != 2)
                        retData = "For correct usage, see ${botdata.botDetails.specialChar}desc whichPlugin."
                    else {
                        retData = "Plugin ${strList.get(1)} is part of masterCommand " +
                                "${AghoraPluginsList.retMasterCommand("whichPlugin")}."
                        type = 1
                        command = IRCCommands.SendPrivMsg.toString()
                        timeToAct = 0L
                    }
                }

                // ,basic setChannel Freenode ##llamas allowed
                /*
                else if(subCommand == "setChannel") {
                    if(size == 4) {
                        if(strList.get(3) == "allowed") {
                            retData = "Channel ${strList.get(1)} allowed to use this command"
                            pluginConfig.setChannelAttributes(true,strList.get(2),strList.get(1))
                        } else if(strList.get(3) == "denied") {
                            pluginConfig.setChannelAttributes(false,strList.get(2),strList.get(1))
                            retData = "Channel ${strList.get(1)} banned from using this command"
                        } else {
                            retData = "Usage: ${specialChar}basic setChannel server ##channel allowed/denied"
                        }
                    }
                    else
                        retData = "Usage: ${specialChar}basic setChannel server ##channel allowed/denied"
                }
                */
                // ,basic setMask Freenode channel prefix plugin yes
                // Prefix can be any of this:-
                // *!*@host
                // *!user@host
                // nick!user@host
                //
                /*
                else if(subCommand == "setMask") {
                    if(size == 5) {
                        for (chan in channelList) {
                            for (prefix in chan.userList) {
                                if (strList.get(3) == "*!*@*") {
                                    for (prefix in chan.userList) {
                                        println("In other condition")
                                        if (strList.get(5).equals("yes")) {
                                            println("Yes")
                                                pluginConfig.setMasksAttributes(true, strList.get(1), chan.retName(), prefix.getData(), strList.get(4))
                                            } else if (strList.get(5).equals("no")) {
                                                println("No")
                                                pluginConfig.setMasksAttributes(false, strList.get(1), chan.retName(), prefix.getData(), strList.get(4))
                                            }
                                            retData = "Set permissions for all in ${chan.retName()} for ${strList.get(5)} to ${strList.get(4)}"
                                        }
                                    }

                                    else if (strList.get(i + 3) != "*!*@*" && prefixMatchesString(strList.get(3), prefix)) {
                                        println("Found")
                                        if (strList.get(i + 5).equals("yes")) {
                                            pluginConfig.setMasksAttributes(true, strList.get(1), strList.get(2), prefix.getData(), strList.get(i + 4))
                                            retData = "Set permissions for ${prefix.getData()} in ${chan.retName()}"
                                        } else if (strList.get(i + 5).equals("no")) {
                                            pluginConfig.setMasksAttributes(false, strList.get(1), strList.get(i + 2), prefix.getData(), strList.get(i + 4))
                                            retData = "Set permissions for ${prefix.getData()} in ${chan.retName()}"
                                        }
                                    }

                                    else
                                        retData = "Usage: ${specialChar}<plugin group> setMask server channel nick!user@host plugin yes/no. Patterns for prefix are supported."
                                }
                            }
                        }
                        else
                            retData = "Usage: ${specialChar}<plugin group> setMask server channel nick!user@host plugin yes/no. Patterns for prefix are supported."
                    }
                    */

                    else if(subCommand == "list") {
                         var botCommands =""
                         var plgList = ""

                        for(j in 0..pluginList.getPluginSubCommands().size - 1) {
                            if(j == 0)
                                plgList = plgList + AghoraPluginsList.getPluginSubCommands().get(j)
                            else if(j == AghoraPluginsList.getPluginSubCommands().size -1)
                                plgList = plgList + " and " + AghoraPluginsList.getPluginSubCommands().get(j) + "."
                            else
                                plgList = plgList + ", " + AghoraPluginsList.getPluginSubCommands().get(j)
                        }

                        for(i in 0..pluginList.botCommands.size - 1) {
                            if(i == 0)
                                botCommands = botCommands + AghoraPluginsList.botCommands.get(i)
                            else if(i == AghoraPluginsList.botCommands.size -1)
                                botCommands = botCommands + " and " + AghoraPluginsList.botCommands.get(i) + "."
                            else
                                botCommands = botCommands + ", " + AghoraPluginsList.botCommands.get(i)
                        }

                        retData = "Bot command list is $botCommands And plugin command list is $plgList"
                        type = 1
                        command = IRCCommands.SendPrivMsg.toString()
                        timeToAct = 0L
                    }

                    // ,desc moo
                    else if(subCommand == "desc") {
                        if(size == i+1 || size == 1)
                            retData = "Utility plugin command to help in describing other plugins. " +
                                    "Use ${botdata.botDetails.specialChar}desc plus a plugin command from " +
                                    "${botdata.botDetails.specialChar}list to help in description. Example " +
                                    "${botdata.botDetails.specialChar}desc moo."
                        if(AghoraPluginsList.subCommandIsThere(strList.get(1))) {
                            if(strList.get(i +1) == "moo")
                                retData = "Fancy plugin written to amuse me when im bored." +
                                    "It is used in the following way: ${botdata.botDetails.specialChar}moo."
                            else if(strList.get(i+1) == "list")
                                retData = "Returns the list of plugins available in the bot at the present moment. Usage: ${botdata.botDetails.specialChar}list"
                            else if(strList.get(i+1) == "setMask")
                                retData = "Ban the hostname/prefix/user from using this group of plugins." +
                                    "Usage: ${botdata.botDetails.specialChar}setMask server channel nick!user@host plugin yes/no. Prefix can be a wildcard expression."
                            else if(strList.get(i+1) == "setChannel")
                                retData = "Set a group of plugin's availability in a particular channel." +
                                    "Usage: ${botdata.botDetails.specialChar}setChannel ##channel allowed/denied"
                            else if(strList.get(i+1) == "source")
                                retData = "Returns the url where this bot is hosted. Try ${botdata.botDetails.specialChar}source"
                            else if(strList.get(i+1) == "version")
                                retData = "Returns the version number of this bot. Try ${botdata.botDetails.specialChar}version"
                            else if(strList.get(i+1) == "whichPlugin")
                                retData = "Returns the masterCommand of a subCommand(like moo). " +
                                        "Try ${botdata.botDetails.specialChar}whichPlugin subCommand. subCommand may be" +
                                        " found from ${botdata.botDetails.specialChar}list"
                            else retData = "Unable to find plugin. Try ${botdata.botDetails.specialChar}list for a complete list."
                        }

                        type = 1
                        timeToAct = 0L
                    } // desc
            }
        } catch (e: Throwable) {
            e.printStackTrace()
        }

        val data = AghoraRetData(retData,type,timeToAct,command,toActOrNot = false)
        return data
    }

    lateinit var configPath: String
}
