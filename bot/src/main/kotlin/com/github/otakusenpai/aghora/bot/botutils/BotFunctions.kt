package com.github.otakusenpai.aghora.bot.botutils

import com.github.otakusenpai.aghora.bot.bot.IRCBot
import com.github.otakusenpai.aghora.bot.botutils.configs.AghoraConfig
import com.github.otakusenpai.aghora.bot.botutils.plugin.AghoraPluginManager
import com.github.otakusenpai.aghora.commonUtils.botdata.BotDetails
import com.github.otakusenpai.aghora.commonUtils.hasIt
import com.github.otakusenpai.aghora.irc.*
import com.github.otakusenpai.aghora.irc.connection.Connection
import com.github.otakusenpai.aghora.irc.hasCommand
import com.github.otakusenpai.aghora.irc.ircmessage.*



fun reloadConfig(msg: IRCMessage, botDetails: BotDetails, aghoraConfig: AghoraConfig, conn: Connection) {
    val data = if(hasIt(msg.retContent(), ':')) {
        msg.retContent().substring(msg.retContent().indexOf(':')+1,
                msg.retContent().length)
    } else {
        msg.retContent()
    }
    val msgList = data.split("\\s{1,}".toRegex())
    if(msgList.get(0) == botDetails.specialChar + "reload")
        aghoraConfig.reloadConfig()
    SendPrivMsg(msg.retSender(), "Reloaded config!!", conn)
}

fun setAccess(tempContent: String, msg: IRCMessage, server: String, aghoraConfig: AghoraConfig,
              botDetails: BotDetails, chanList: MutableList<Channel>, conn: Connection) {
    if(aghoraConfig.getUserAllowedBotCommands(server,msg.retSender(),msg.retPrefix().getData(),"set") ||
            prefixMatchesString(aghoraConfig.getOwner(),msg.retPrefix())) {
        val msgList = tempContent.split("\\s{1,}".toRegex())
        aghoraConfig.reloadConfig()
        if(msgList.size < 3)
            SendPrivMsg(msg.retSender(),
                    "Usage: ${botDetails.specialChar}set command nick yes/no", conn)
        else {
            val plugin = msgList.get(1)
            val nick = msgList.get(2)
            var foundNickInChannel = false
            var foundChanInChanList = false

            for(i in chanList) {
                if(msg.retSender() == i.retName()) {
                    foundChanInChanList = true
                    if(i.userInChannel(nick)) {
                        foundNickInChannel = true
                        if(prefixMatchesString(aghoraConfig.getOwner(),msg.retPrefix())) {
                            if (msgList.get(3) == "yes") {
                                aghoraConfig.setUserAllowedBotCommands(server, i.retName(),
                                        i.getPrefix(nick).getData(), plugin, true)
                                SendPrivMsg(msg.retSender(), "Done setting.", conn)
                                aghoraConfig.flush()
                                reloadConfig(msg,botDetails,aghoraConfig,conn)
                            } else if (msgList.get(3) == "no") {
                                aghoraConfig.setUserAllowedBotCommands(server, i.retName(),
                                        i.getPrefix(nick).getData(), plugin, false)

                                SendPrivMsg(msg.retSender(), "Done setting.", conn)
                                aghoraConfig.flush()
                                reloadConfig(msg,botDetails,aghoraConfig,conn)
                            } else SendPrivMsg(msg.retSender(),
                                    "Usage: ${botDetails.specialChar}set command nick yes/no", conn)
                        } else {
                            if (msgList.get(3) == "yes") {
                                aghoraConfig.setUserAllowedBotCommands(server, i.retName(),
                                        i.getPrefix(nick).getData(), plugin, true)
                                SendPrivMsg(msg.retSender(), "Done setting.", conn)
                                aghoraConfig.flush()
                                reloadConfig(msg,botDetails,aghoraConfig,conn)
                            } else if (msgList.get(3) == "no") {
                                aghoraConfig.setUserAllowedBotCommands(server, i.retName(),
                                        i.getPrefix(nick).getData(), plugin, false)

                                SendPrivMsg(msg.retSender(), "Done setting.", conn)
                                aghoraConfig.flush()
                                reloadConfig(msg,botDetails,aghoraConfig,conn)
                            } else SendPrivMsg(msg.retSender(),
                                    "Usage: ${botDetails.specialChar}set command nick yes/no", conn)
                        }
                    }
                }
            }
            if (!foundNickInChannel)
                SendPrivMsg(msg.retSender(), "$nick not found in channel.", conn)
            if(!foundChanInChanList)
                SendPrivMsg(msg.retSender(), "${msg.retSender()} not in channel list.", conn)
        }
    } else  {
        SendPrivMsg(msg.retSender(), "${msg.retPrefix().nick} is not allowed to use this command", conn)
    }
}

fun loadPlugin(server: String, msg: IRCMessage, tempContent: String, botDetails: BotDetails, bot: IRCBot,
               aghoraConfig: AghoraConfig, pluginManager: AghoraPluginManager, conn: Connection) {
    if(msg.retContent().length <= 1 || msg.retContent().length != 2) {
        SendPrivMsg(msg.retSender(),"Usage: ${botDetails.specialChar}load plugin-jar-name.jar. " +
                "For example ${botDetails.specialChar}load BasicOps.jar.",conn)
    } else {
        if(bot.config.getUserAllowedBotCommands(server,msg.retSender(),msg.retPrefix().getData(),"load") ||
                prefixMatchesString(aghoraConfig.getOwner(),msg.retPrefix())) {
            val msgList = tempContent.split("\\s{1,}".toRegex())
            if((msgList.size-1) == 1){
                val plugin = msgList.get(1)
                for((jarName,config) in bot.config.getPluginList()) {
                    if(plugin == jarName)
                        pluginManager.loadPlugin(jarName,config,bot)
                }
                SendPrivMsg(msg.retSender(), "Loaded ${plugin} plugin.", conn)
            } else
                SendPrivMsg(msg.retSender(),
                        "Usage: ${bot.ircData.botDetails.specialChar}load BasicOps.jar (example)", conn)
        } else  {
            SendPrivMsg(msg.retSender(), "${msg.retPrefix().nick} is not allowed to use this command", conn)
        }
    }
}

fun unloadPlugin(server: String, msg: IRCMessage, tempContent: String, botDetails: BotDetails, bot: IRCBot,
                 aghoraConfig: AghoraConfig, pluginManager: AghoraPluginManager, conn: Connection) {
    if(msg.retContent().length <= 1 || msg.retContent().length != 2) {
        SendPrivMsg(msg.retSender(),"Usage: ${botDetails.specialChar}unload plugin-jar-name " +
                "(without '.jar'). For example ${botDetails.specialChar}unload BasicOps.",conn)
    } else {
        if(bot.config.getUserAllowedBotCommands(server,msg.retSender(),msg.retPrefix().getData(),"unload") ||
                prefixMatchesString(aghoraConfig.getOwner(),msg.retPrefix())) {
            val msgList = tempContent.split("\\s{1,}".toRegex())
            val plugin = msgList[1]
            for((pluginName,masterCommand) in pluginManager.pluginList) {
                println(pluginName)
                if(plugin == pluginName) {
                    pluginManager.unloadPlugin(plugin)
                    SendPrivMsg(msg.retSender(), "Unloaded ${plugin} plugin.", conn)
                }
            }
        } else
            SendPrivMsg(msg.retSender(), "${msg.retPrefix().nick} is not allowed to use this command", conn)
    }
}

fun channelRelated(msg: IRCMessage,chanList: MutableList<Channel>) {
    if(hasCommand(msg, PacketType.RPL_TOPIC.toString())) {
        val message = msg.retContent()
        var chan = msg.retSender()
        var topic = message.substring(message.indexOf(':')+1,
                message.length)
        for(i in chanList) {
            if(i.retName() == chan) {
                i.setTopic(topic)
                println("Channel: ${i.retName()}")
                break
            }
        }
    } else if(hasCommand(msg, PacketType.RPL_NAMREPLY.toString())) {
        // TODO here
    }
}

fun Part(data: String, chanList: MutableList<Channel>, conn: Connection) {
    var total = data.substring(data.indexOf(" ")+1)
    lateinit var chan: String
    var message: String = ""
    // If the first literal is space
    if(total.get(0) == ' ') {
        total = total.substring(total.indexOf(' '),total.length)
    }

    // If message has to be send with part
    if(hasIt(total, ' ')) {
        chan = total.substring(0,total.indexOf(" "))
        message = total.substring(total.indexOf(" ")+1,total.length)
    } else { chan = total }

    // Sending part
    if(message.isEmpty()) {
        println("Sending part to ${chan}")
        SendPart(chan, conn)
    }
    else if(!message.isEmpty()) {
        println("Sending part to ${chan}")
        println("Part message is ${message}")
        SendPart(chan, message, conn)
    }

    // Remove the channel from the chanList
    var pos = 0
    for(i in chanList) {
        ++pos
        if(i.retName() == chan) {
            chanList.removeAt(pos)
        }
    }
}

fun Join(data: String, chanList: MutableList<Channel>, conn: Connection) {
    var channel = data.substring(data.indexOf(" ")+1,data.length).trimEnd()
    JoinChannel(channel, conn)
    chanList.add(Channel(channel))
}