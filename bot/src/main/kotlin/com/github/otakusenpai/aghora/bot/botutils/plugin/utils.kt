package com.github.otakusenpai.aghora.bot.botutils.plugin

import com.github.otakusenpai.aghora.commonUtils.error.AghoraException
import com.github.otakusenpai.aghora.commonUtils.plugin.PlgCommand
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.channels.Channel

suspend fun retPlgCommands(data: String): PlgCommand {
    val channel = Channel<PlgCommand>()
    val foo = async {
        if(data == PlgCommand.moo.toString())
            channel.send(PlgCommand.moo)
        else if(data == PlgCommand.source.toString())
            channel.send(PlgCommand.source)
        else if(data == PlgCommand.list.toString())
            channel.send(PlgCommand.list)
        else if(data == PlgCommand.desc.toString())
            channel.send(PlgCommand.desc)
        else if(data == PlgCommand.setMask.toString())
            channel.send(PlgCommand.setMask)
        else if(data == PlgCommand.setChannel.toString())
            channel.send(PlgCommand.setChannel)
        else if(data == PlgCommand.kick.toString())
            channel.send(PlgCommand.kick)
        else if(data == PlgCommand.invite.toString())
            channel.send(PlgCommand.invite)
        else if(data == PlgCommand.limit.toString())
            channel.send(PlgCommand.limit)
        else if(data == PlgCommand.ban.toString())
            channel.send(PlgCommand.ban)
        else
            channel.send(PlgCommand.none)
    }

    var ircSubCommand = channel.receive()

    if(ircSubCommand == PlgCommand.none)
        throw AghoraException("Error: At retPlgCommands() in com.github.otakusenpai.aghora.bot.botutils.plugin, " +
                "No match for subCommand $data !")

    return ircSubCommand
}