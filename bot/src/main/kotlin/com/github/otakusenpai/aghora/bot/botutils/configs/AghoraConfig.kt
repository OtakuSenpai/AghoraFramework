package com.github.otakusenpai.aghora.bot.botutils.configs

import com.github.otakusenpai.aghora.commonUtils.botdata.BotDetails
import nu.xom.*
import java.io.File
import java.io.FileOutputStream

class AghoraConfig {

    constructor(path: String) {
        pathToXml = path
        var xmlBuilder = Builder()
        var f = File(path)
        xmlDocument = xmlBuilder.build(f)
        rootElement = xmlDocument.rootElement
    }

    fun reloadConfig() {
        var xmlBuilder = Builder()
        var f = File(pathToXml)
        xmlDocument = xmlBuilder.build(f)
        rootElement = xmlDocument.rootElement
    }

    fun getServerDetails(server: String) : BotDetails = BotDetails(getOwner(),getFirstNick(server), getSecondNick(server),
            getUser(server), getRealname(server), getSpecialChar(server), getPort(server), getAddress(server),
            getSsl(server), getPasswdUsed(server), getPassword(server), getBotCommands())

    fun getBotCommands() : List<String> {
        var commandList = mutableListOf<String>()

        try {
            val s = rootElement.getFirstChildElement("botCommands").childElements
            for (i in 0..(s.size() - 1)) {
                commandList.add(s.get(i).localName)
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }

        return commandList
    }

    // First one is the server name
    // Second one is the type of bot(IRC/Discord)
    // Third one is the bot identifier for the server
    fun getServerAndTypeList() : List<Triple<String,String,String>> {
        var serverList = mutableListOf<Triple<String,String,String>>()
        try {
            val s = rootElement.getFirstChildElement("serverlist").childElements
            for(i in 0..(s.size()-1)) {
                var name = s.get(i).getAttributeValue("name")
                var type = s.get(i).getAttributeValue("type")
                var botname = s.get(i).getAttributeValue("botname")
                serverList.add(Triple(name,type,botname))
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }

        return serverList
    }

    fun getOwner(): String {
        lateinit var owner: String
        try {
            var owner1 = rootElement.getFirstChildElement("owner")
            owner = owner1.value
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return owner
    }

    fun getFirstNick(network: String) : String {
        var name = ""
        try {
            val serverlist = rootElement.
                    getFirstChildElement("serverlist").childElements
            for(i in 0..(serverlist.size()-1)) {
                if(serverlist.get(i).getAttribute("name").value == network) {
                    var name1 = serverlist.get(i).getFirstChildElement("nickone")
                    name = name1.value
                }
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return name
    }

    fun getSecondNick(network: String) : String {
        var name = ""
        try {
            val serverlist = rootElement.
                    getFirstChildElement("serverlist").childElements
            for(i in 0..(serverlist.size()-1)) {
                if(serverlist.get(i).getAttribute("name").value == network) {
                    var name1 = serverlist.get(i).getFirstChildElement("nicktwo")
                    name = name1.value
                }
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return name
    }

    fun getUser(network: String) : String {
        var name = ""
        try {
            val serverlist = rootElement.
                    getFirstChildElement("serverlist").childElements
            for(i in 0..(serverlist.size()-1)) {
                if(serverlist.get(i).getAttribute("name").value == network) {
                    var name1 = serverlist.get(i).getFirstChildElement("user")
                    name = name1.value
                }
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return name
    }

    fun getRealname(network: String) : String {
        var name = ""
        try {
            val serverlist = rootElement.
                    getFirstChildElement("serverlist").childElements
            for(i in 0..(serverlist.size()-1)) {
                if(serverlist.get(i).getAttribute("name").value == network) {
                    var name1 = serverlist.get(i).getFirstChildElement("realname")
                    name = name1.value
                }
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return name
    }

    fun getPort(network: String) : Int {
        var name = 0
        try {
            val serverlist = rootElement.
                    getFirstChildElement("serverlist").childElements
            for(i in 0..(serverlist.size()-1)) {
                if(serverlist.get(i).getAttribute("name").value == network) {
                    var name1 = serverlist.get(i).getFirstChildElement("port")
                    name = name1.value.toInt()
                }
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return name
    }

    fun getAddress(network: String) : String {
        var name = ""
        try {
            val serverlist = rootElement.
                    getFirstChildElement("serverlist").childElements
            for(i in 0..(serverlist.size()-1)) {
                if(serverlist.get(i).getAttribute("name").value == network) {
                    var name1 = serverlist.get(i).getFirstChildElement("address")
                    name = name1.value
                }
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return name
    }

    fun getSsl(network: String) : Boolean {
        var name = false
        try {
            val serverlist = rootElement.
                    getFirstChildElement("serverlist").childElements
            for(i in 0..(serverlist.size()-1)) {
                if(serverlist.get(i).getAttribute("name").value == network) {
                    var name1 = serverlist.get(i).getFirstChildElement("ssl")
                    name = name1.value == "yes"
                }
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return name
    }

    fun getPasswdUsed(network: String) : Boolean {
        var name = false
        try {
            val serverlist = rootElement.
                    getFirstChildElement("serverlist").childElements
            for(i in 0..(serverlist.size()-1)) {
                if(serverlist.get(i).getAttribute("name").value == network) {
                    var name1 = serverlist.get(i).getFirstChildElement("passwdUsed")
                    name = name1.value == "yes"
                }
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return name
    }

    fun getSpecialChar(network: String) : Char {
        var name = ' '
        try {
            val serverlist = rootElement.
                    getFirstChildElement("serverlist").childElements
            for(i in 0..(serverlist.size()-1)) {
                if(serverlist.get(i).getAttribute("name").value == network) {
                    var name1 = serverlist.get(i).getFirstChildElement("specialChar")
                    name = name1.value.get(0)
                }
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return name
    }

    fun getPassword(network: String) : String {
        var name = ""
        try {
            val serverlist = rootElement.
                    getFirstChildElement("serverlist").childElements
            for(i in 0..(serverlist.size()-1)) {
                if(serverlist.get(i).getAttribute("name").value == network) {
                    var name1 = serverlist.get(i).getFirstChildElement("password")
                    name = name1.value
                }
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return name
    }

    fun getPluginDir() : String {
        var name = ""
        try {
            var name1 = rootElement.getFirstChildElement("pluginDir")
            name = name1.value
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return name
    }

    fun getConfigDir() : String {
        var name = ""
        try {
            var name1 = rootElement.getFirstChildElement("configDir")
            name = name1.value
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return name
    }

    fun getConfigPath(pluginName: String) : String {
        val plgMap = getPluginList()
        var configPath = ""
        for((jarName, config) in plgMap) {
            if(jarName == (pluginName + ".jar")) {
                configPath = config
                break
            }
        }

        return configPath
    }

    // Returns a Map of jar name to config path values

    fun getPluginList() : Map<String,String> {
        var childList: MutableMap<String,String> = mutableMapOf()

        try {
            var childrenList = rootElement.
                    getFirstChildElement("groups").childElements
            for(i in 0..(childrenList.size()-1)) {
                val jarName = childrenList.get(i).getFirstChildElement("jar")
                val configPath = childrenList.get(i).getFirstChildElement("config")
                childList[jarName.value] = configPath.value
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
        return childList
    }

    // returns whether user allowed to use bot command

    fun getUserAllowedBotCommands(network: String,channel: String, prefix: String,plugin: String) : Boolean {
        var found  = false
        try {
            val botPlugins = rootElement.
                    getFirstChildElement("botPlugins").childElements

            // networks
            for(i in 0..(botPlugins.size()-1)) {
                if(botPlugins.get(i).getAttributeValue("name") == network) {
                    val channels = botPlugins.get(i).childElements
                    //channels
                    for(i in 0..(channels.size()-1)) {
                        if(channels.get(i).getAttributeValue("name") == channel) {
                            val prefixes = channels.get(i).childElements
                            //prefixes
                            for(i in 0..(prefixes.size()-1)) {
                                if(prefixes.get(i).getAttributeValue("ident") == prefix) {
                                    val plugins = prefixes.get(i).childElements
                                    //plugins
                                    for(i in 0..(plugins.size()-1)) {
                                        if (plugins.get(i).value == "yes")
                                            found = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }

        return found
    }

    // sets whether a user is allowed to use a bot command

    fun setUserAllowedBotCommands(network: String,channel: String, prefix: String,plugin: String,allowed: Boolean) {
        try {
            val pluginToAdd = Element("plugin")
            pluginToAdd.addAttribute(Attribute("name",plugin))
            if(allowed)
                pluginToAdd.appendChild("yes")
            else
                pluginToAdd.appendChild("no")

            var foundChan = false
            var foundPrefix = false
            var foundPlugin = false
            var foundNetwork = false

            val botPluginList = rootElement.
                    getFirstChildElement("botPlugins")
            val botPlugins = rootElement.
                    getFirstChildElement("botPlugins").childElements
            //network
            for(i in 0..(botPlugins.size()-1)) {
                if(botPlugins.get(i).getAttributeValue("name") == network) {
                    foundNetwork = true
                    val channels = botPlugins.get(i).childElements
                    val channelList = botPlugins.get(i)
                    //channels
                    for(i in 0..(channels.size()-1)) {
                        if(channels.get(i).getAttributeValue("name") == channel) {
                            foundChan = true
                            val prefixes = channels.get(i).childElements
                            val prefixesList = channels.get(i)
                            //prefixes
                            for(i in 0..(prefixes.size()-1)) {
                                if(prefixes.get(i).getAttributeValue("ident") == prefix) {
                                    foundPrefix = true
                                    val plugins = prefixes.get(i).childElements
                                    val plugins2 = prefixes.get(i)
                                    //plugins
                                    for(j in 0..(plugins.size()-1)) {
                                        if(plugins.get(j).getAttributeValue("name") == plugin) {
                                            foundPlugin = true
                                            plugins2.removeChild(plugins.get(j))
                                            plugins2.insertChild(pluginToAdd,j)
                                        }
                                    }
                                    if(foundNetwork && foundChan && foundPrefix && !foundPlugin) {
                                        plugins2.appendChild(pluginToAdd)
                                    }
                                }
                            }
                            if(foundNetwork && foundChan && !foundPrefix) {
                                val prefixToAdd = Element("prefix")
                                prefixToAdd.addAttribute(Attribute("ident",prefix))

                                prefixToAdd.appendChild(pluginToAdd)
                                prefixesList.appendChild(prefixToAdd)
                            }
                        }
                    }
                    if(foundNetwork && !foundChan) {
                        val channelToAdd = Element("channel")
                        channelToAdd.addAttribute(Attribute("name",channel))
                        val prefixToAdd = Element("prefix")
                        prefixToAdd.addAttribute(Attribute("ident",prefix))

                        prefixToAdd.appendChild(pluginToAdd)
                        channelToAdd.appendChild(prefixToAdd)
                        channelList.appendChild(channelToAdd)
                    }
                }
            }
            if(!foundNetwork) {
                val netToAdd = Element("network")
                netToAdd.addAttribute(Attribute("name",network))
                val channelToAdd = Element("channel")
                channelToAdd.addAttribute(Attribute("name",channel))
                val prefixToAdd = Element("prefix")
                prefixToAdd.addAttribute(Attribute("ident",prefix))

                prefixToAdd.appendChild(pluginToAdd)
                channelToAdd.appendChild(prefixToAdd)
                netToAdd.appendChild(channelToAdd)
                botPluginList.appendChild(netToAdd)
            }
            writeDoc(xmlDocument)
        } catch(e: Throwable) {
            e.printStackTrace()
        } catch(e: ParsingException) {
            e.printStackTrace()
        }
    }

     private fun writeDoc(doc: Document) {
        try {
            var f = FileOutputStream(pathToXml)
            var serializer = Serializer(f,"UTF-8")
            serializer.indent = 4
            serializer.maxLength = 80
            serializer.write(xmlDocument)
            serializer.flush()
            f.flush()
            f.close()
            reloadConfig()
        } catch(e: Throwable) {
            e.printStackTrace()
        }
    }

    fun flush() {
        writeDoc(xmlDocument)
    }

    var xmlDocument: Document
    var rootElement: Element
    var pathToXml: String = ""
}