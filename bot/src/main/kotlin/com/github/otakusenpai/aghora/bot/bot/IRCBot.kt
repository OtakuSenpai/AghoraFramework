package com.github.otakusenpai.aghora.bot.bot

import com.github.otakusenpai.aghora.bot.botutils.*
import com.github.otakusenpai.aghora.bot.botutils.configs.AghoraConfig
import com.github.otakusenpai.aghora.bot.botutils.plugin.AghoraPluginManager
import com.github.otakusenpai.aghora.bot.botutils.time.AghoraTimeKeeper
import com.github.otakusenpai.aghora.commonUtils.error.AghoraException
import com.github.otakusenpai.aghora.commonUtils.hasIt
import com.github.otakusenpai.aghora.commonUtils.log.AsyncLogger
import com.github.otakusenpai.aghora.commonUtils.log.getThreadName
import com.github.otakusenpai.aghora.commonUtils.log.log
import com.github.otakusenpai.aghora.commonUtils.log.logMessages
import com.github.otakusenpai.aghora.commonUtils.plugin.AghoraPluginsList
import com.github.otakusenpai.aghora.commonUtils.random
import com.github.otakusenpai.aghora.commonUtils.time.createAghoraTime
import com.github.otakusenpai.aghora.irc.*
import com.github.otakusenpai.aghora.irc.connection.*
import com.github.otakusenpai.aghora.irc.ircmessage.*
import com.github.otakusenpai.aghora.pluginUtils.IRCBotData
import com.github.otakusenpai.aghora.pluginUtils.api.AghoraInterface
import io.netty.util.Timer
import kotlin.concurrent.schedule

import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.channels.TickerMode
import kotlinx.coroutines.experimental.channels.ticker
import kotlinx.coroutines.experimental.time.withTimeoutOrNull
import java.time.LocalTime
import java.util.*

internal const val toFile = false
internal const val toLog = true

open abstract class IRCBot : AbstractBot {

    /**
     * @param configPath The path to the bot's XML config.
     * @param serverName The server name of this bot as defined
     * in the config.
     * @param botname The bot's name as defined in the config.
     * @return IRCBot
     * @since 0.1.7.1
     * Base constructor of the IRCBot class.
     */
    constructor(configPath: String, serverName: String,botname: String) :
            super(configPath, serverName,botname) {
        ircData = IRCBotData(mutableListOf(),config.getServerDetails(serverName))
        commandList = config.getBotCommands()
        if (ircData.botDetails.useSSL) this.conn = SslConnection(ircData.botDetails.port, ircData.botDetails.address)
        else if (!ircData.botDetails.useSSL) this.conn = BasicConnection(ircData.botDetails.port, ircData.botDetails.address)
        floodProtector = IRCFloodProtector(conn,6,false)
        logger = AsyncLogger("./logs",botname + '-' + (0..1000).random().toString() + ".txt")
    }

    /**
     * @param msg The message received from the exception.
     * @return Unit
     * @since 0.1.7.1
     * Used to send exceptions received to the owner of this bot.
     */
    inline fun sentErrorDataTo(msg: String?) {
        for(chan in ircData.chanList) {
            for(prefix in chan.retUserList())
                if(prefixMatchesString(ircData.botDetails.owner,prefix))
                    SendPrivMsg(prefix.nick, msg as String, conn)
        }
    }

    /**
     * @param data The message received from the server.
     * @return MutableList<IRCMessage>
     * @since 0.1.x
     * Used to seperate a list of messages to a MutableList of
     * IRCMessages.
     */
    fun segragate(data: String?): MutableList<IRCMessage> {
        // logger.log(toLog, toFile,"${Thread.currentThread().name}: $botname: I'm now in segragate!")
        var tempList = mutableListOf<IRCMessage>()

        if(data != null) {
            // log("Inside if statement", logMessages)
            var i = 0
            for(msg in data.split("\r\n")) {
                // logger.log(toLog, toFile,botname,"Loop $i")
                tempList.add(IRCMessage(msg))
            }
            // logger.log(toLog, toFile,"${Thread.currentThread().name}: $botname: done separating")
        } else
            throw AghoraException("Error: At segragate(String?) in " +
                    "com.github.otakusenpai.aghora.bot.bot.IRCBot:  Can't segragate null string.")
        // logger.log(toLog, toFile,"${Thread.currentThread().name}: $botname: return from segragate")
        return tempList
    }

    /**
     * @return Unit
     * @since 0.1.7.1
     * Used to load all the plugins from the plugins directory.
     */
    fun loadPlugins() {
        this.pluginManager = AghoraPluginManager(config.getPluginDir())
        this.pluginManager.loadPlugins(this)
        this.aghoraPluginList.botCommands = this.commandList as MutableList<String>
    }

    /**
     * @return Unit
     * @since 0.1.x
     * Used to set the connection to the server.
     */
    override suspend fun setConnection() {
        try {
            logger.log(toLog, toFile,"$botname: Connecting....")
            var i = 0
            while(!conn.connected) {
                ++i
                delay(6000L)
                logger.log(toLog, toFile,"$botname: Trying for the ${i}'th time... ")
                conn.Connect()
            }

            logger.log(toLog, toFile,"$botname: Sending nick...")
            SendNick(ircData.botDetails.nick1, conn)

            logger.log(toLog, toFile,"$botname: Sending user...")
            SendUser(ircData.botDetails.user, ircData.botDetails.realname, 0, conn)

            logger.log(toLog, toFile,"$botname: Connected!")
            running = true
        } catch(e: Throwable) {
            throw e
            running = false
        }
    }

    /**
     * @return Unit
     * @since 0.1.x
     * The bot's main loop, keeps it running.
     */
    override suspend fun Connect() = coroutineScope {
        try {
            logger.log(toLog, toFile,"$botname: I'm now in Connect!")
            var foundMOTD = false
            lateinit var tmpList: MutableList<IRCMessage>
            var data: String?
            var passwdSend = false
            val whoUpdater = ticker(50000L,0,this.coroutineContext,TickerMode.FIXED_PERIOD)

            loadPlugins()

            logger.log(toLog, toFile,"$botname: Entering loop...")
            while(running) {
                val one = async {
                    data = conn.receiveUTF8Data()
                    logger.log(toLog, toFile,"$botname: Received data: $data")
                    tmpList = segragate(data)
                }
                one.await()

                for(i in tmpList) {
                    logger.log(toLog, toFile,"$botname: Inside for loop")
                    val tempContent = if(hasIt(i.retContent(),':')) {
                        i.retContent().substring(i.retContent().indexOf(':')+1,
                                i.retContent().length)
                    } else {
                        i.retContent()
                    }
                    if(hasCommand(i,"004")) {
                        logger.log(toLog, toFile,"$botname: Connected to server...")
                    }
                    if(hasCommand(i,"433")) {
                        if(nickValue) SendNick(ircData.botDetails.nick1, conn)
                        else SendNick(ircData.botDetails.nick2,conn)
                    }
                    if(hasCommand(i,"376")) {
                        foundMOTD = true
                        if(!passwdSend) {
                            if (ircData.botDetails.passwdUsed) {
                                SendNickservIdentify(ircData.botDetails.password, conn)
                                passwdSend = true
                            }
                        }
                        JoinChannel("#AghoraBot", conn)
                        ircData.chanList.add(Channel("#AghoraBot"))
                    }

                    if (i.retCommand() == "PING") {
                        SendPong(i.retContent(), conn)
                    }

                    if(foundMOTD && passwdSend) {
                        messageHandlers(tmpList)

                        if (hasIt(tempContent, (ircData.botDetails.specialChar + "join"))) {
                            Join(tempContent, ircData.chanList, conn)
                        }
                        if (hasIt(tempContent, (ircData.botDetails.specialChar + "part"))) {
                            Part(tempContent, ircData.chanList, conn)
                        }
                        if (hasCommand(i, PacketType.RPL_NAMREPLY.toString())  ||
                                hasCommand(i, PacketType.RPL_TOPIC.toString())) {
                            channelRelated(i, ircData.chanList)
                            // floodProtector.addMessage(i.retSender(),SendWho(i.retSender()))
                        }
                        if(hasCommand(i, PacketType.RPL_WHOREPLY.toString()) ||
                                hasCommand(i, PacketType.RPL_ENDOFWHO.toString())) {
                            if(hasCommand(i, PacketType.RPL_ENDOFWHO.toString()))
                                continue
                            else {
                                for (j in ircData.chanList) {
                                    j.addUserToChan(i)
                                }
                            }
                        }
                        if (hasIt(tempContent, (ircData.botDetails.specialChar + "quit"))) {
                            conn.Disconnect()
                            running = false
                        }
                    }

                    launch {
                        for(delay in whoUpdater) {
                            logger.log(toLog, toFile,"$botname: Turning on channel sync")
                            updateChanList()
                            logger.log(toLog, toFile,"$botname: Done syncing")
                            break
                        }
                    }
                }
            }

        } catch(e: Exception) {
            sentErrorDataTo(e.message)
            logger.log(toLog, toFile,"$botname: " + e.message!!)
            throw AghoraException("Error: $botname has closed!",e)
        }
    }

    private suspend fun updateChanList() {
        for(i in 0 until (ircData.chanList.size-1)) {
            floodProtector.addMessage(ircData.chanList[i].retName(),SendWho(ircData.chanList[i].retName()))
            floodProtector.sendMsg(ircData.chanList[i].retName(),false)
        }
    }

    /**
     * @param data A MutableList of IRCMessages to be processed by various
     * base functions of a IRCBot
     * @return Unit
     * @since 0.1.x
     * This function handles all the basic running features of a bot,
     * from setting plugin access to unloading plugins and more.
     */
    suspend fun messageHandlers(data: MutableList<IRCMessage>) = coroutineScope {
        try {
            logger.log(toLog, toFile,"$botname: I'm now in handleMessage!")

            for (msg in data) {
                val tempContent = if(hasIt(msg.retContent(),':')) {
                    msg.retContent().substring(msg.retContent().indexOf(':')+1,
                            msg.retContent().length)
                } else {
                    msg.retContent()
                }

                var i = 0
                val subCommand = tempContent.substring(1,tempContent.length)

                /*
                if(!aghoraPluginList.subCommandIsThere(subCommand))
                    for(chan in ircData.chanList)
                        if(chan.userInChannel(msg.retSender())) {
                            logger.log(toLog, toFile,botname,"Loop = ${++i}")
                            SendPrivMsg(msg.retSender(),"Command $subCommand isn't implemented!",conn)
                            break
                        }
                */

                // log("Subcommand in handleMessages is $subCommand", logMessages)

                if(AghoraPluginsList.hasPlgCommand(tempContent)) {
                    launch {
                        pluginCall(msg, msg.retSender(), config,
                                pluginManager, conn, subCommand)
                    }
                }

                else if(hasIt(tempContent,ircData.botDetails.specialChar + "setPlugin")) {
                    launch {
                        setPluginAccess(msg, ircData.chanList, pluginManager, conn)
                    }
                }


                // ,reload
                else if(hasIt(tempContent,ircData.botDetails.specialChar + "reload")) {
                    reloadConfig(msg, ircData.botDetails, config, conn)
                    for(chan in ircData.chanList) {
                        SendWho(chan.retName(), conn)
                    }
                }

                // ,set list nick yes/no
                else if(hasIt(tempContent,ircData.botDetails.specialChar + "set")) {
                    setAccess(tempContent, msg, server, config, ircData.botDetails, ircData.chanList, conn)
                }

                // ,load plugin
                else if(hasIt(tempContent,ircData.botDetails.specialChar + "load")) {
                    loadPlugin(server, msg, tempContent,ircData.botDetails,this@IRCBot,config,pluginManager, conn)
                }

                else if(hasIt(tempContent,ircData.botDetails.specialChar + "unload")) {
                    unloadPlugin(server, msg, tempContent,ircData.botDetails,this@IRCBot,config,pluginManager, conn)
                }

                /*
                if(tempContent.length > 2) {

                    //timeKeeper.check(timeKeeper.checkTime(),ircCommand,msgToAct.retSender(),
                    //        msg.ret)


                }
                */
            }

            val three = async { customMsgHandlers(data) }

            three.await()
            // logger.log(toLog, toFile,botname,"Done computing,exiting handleMessage!")
        } catch (e: Exception) {
            throw e
        }
    }

    /**
     * @param data The data list of messages to parse
     * @return Unit
     * @since 0.1.7.1
     * This function handles a user's own functions of a bot implementation.
     */
    abstract suspend fun customMsgHandlers(data: MutableList<IRCMessage>)



    private suspend fun pluginCall(msg: IRCMessage, channel: String, aghoraConfig: AghoraConfig,
                                   pluginManager: AghoraPluginManager, conn: Connection, subCommand: String) {
        val data = msg.retContent().substring(msg.retContent().indexOf(':')+1,
                msg.retContent().length)
        val msgList = data.split("\\s{1,}".toRegex()) as MutableList

        if(AghoraPluginsList.subCommandIsThere(subCommand)) {
            for(pluginEntity in pluginManager.pluginList) {
                logger.log(toLog, toFile,"Found subCommand!")
                val plugin = pluginManager.getPluginInterface(pluginEntity.key)
                        as AghoraInterface
                plugin.init(this@IRCBot.config.getConfigPath(pluginEntity.key),this@IRCBot.ircData)
                if(prefixMatchesString(aghoraConfig.getOwner(), msg.retPrefix())) {
                    logger.log(toLog, toFile,"In pluginCall: Data = $data")
                    logger.log(toLog, toFile,"Message to be processed in pluginCall is: ${msg.retMsgData()}")
                    val data = plugin.onCommand(msg)

                    if(data.type == 1) SendPrivMsg(channel, data.message, conn)
                    else if(data.type == 2) SendRawMsg(data.message, conn)
                }
                else if(plugin.pluginConfig.ifMasksNotAllowed(this@IRCBot.server,channel,msg.retPrefix().getData(),msgList.get(0))) {
                    logger.log(toLog, toFile,"Called")
                    val data = plugin.onCommand(msg)

                    if (data.type == 1) { // channel message
                        SendPrivMsg(channel, data.message, conn)
                        logger.log(toLog, toFile,"Channel message: " + data.message)
                        this@IRCBot.timeKeeper.timeList.add(createAghoraTime(data.timeToAct).toLocalDateTime())
                        // bot.ircCommand = setIRCCommand(data.command)
                        //bot.msgToAct = data.msgdata
                    }
                    else if (data.type == 2) { // server message
                        logger.log(toLog, toFile,"Server message: " + data.message)
                        SendRawMsg(data.message, conn)
                        this@IRCBot.timeKeeper.timeList.add(createAghoraTime(data.timeToAct).toLocalDateTime())
                        // bot.ircCommand = setIRCCommand(data.command)
                        // bot.msgToAct = data.msgdata
                    }
                }
            }
        }
    }

    protected suspend fun setPluginAccess(msg: IRCMessage, chanList: MutableList<Channel>,
                                          pluginManager: AghoraPluginManager, conn: Connection) {
        try {
            val data = msg.retContent().substring(msg.retContent().indexOf(':'),
                    msg.retContent().length)
            val msgList = data.split("\\s{1,}".toRegex())
            var found = false
            val data1 = msgList.get(0).substring(2,msgList.get(0).length)

            if(msgList.size < 5)
                SendPrivMsg(msg.retSender(), "Usage: ${this@IRCBot.ircData.botDetails.specialChar}setPlugin " +
                        "plugin-mastercommand subCommand nick yes/no. Nick has to be in channel.Get the" +
                        " masterCommand from '${this@IRCBot.ircData.botDetails.specialChar}whichPlugin'.", conn)

            if(data1 == "setPlugin") {
                println("Start")
                for((pluginName,mastercommand) in pluginManager.pluginList) {
                    if(msgList.get(1) == mastercommand) { // like chan or basic
                        val plugin = pluginManager.getPluginInterface(pluginName)
                                as AghoraInterface
                        plugin.init(this@IRCBot.config.getConfigPath(pluginName),this@IRCBot.ircData)
                        for(plugins in plugin.subCommands) {
                            if (plugins == msgList.get(2)) {
                                for(i in chanList) {
                                    if(i.retName() == msg.retSender()) {
                                        for(j in i.userList) {
                                            if(prefixMatchesString(config.getOwner(),msg.retPrefix()) ||
                                                    config.getUserAllowedBotCommands(server,i.retName(),j.getData(),msgList.get(2))) {
                                                if(j.nick == msgList.get(3)) {
                                                    if(msgList.get(4) == "yes" || msgList.get(4) == "Yes")
                                                        plugin.pluginConfig.setMasksAttributes(true,
                                                                this@IRCBot.server,i.retName(),j.getData(),msgList.get(2))
                                                    else if(msgList.get(4) == "no" || msgList.get(4) == "No")
                                                        plugin.pluginConfig.setMasksAttributes(false,
                                                                this@IRCBot.server,i.retName(),j.getData(),msgList.get(2))
                                                    found = true
                                                }
                                            } else SendPrivMsg(msg.retSender(),"${msg.retPrefix().nick} is not allowed to use this command", conn)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(!found)
                SendPrivMsg(msg.retSender(), "Usage: ${this@IRCBot.ircData.botDetails.specialChar}setPlugin " +
                        "plugin-mastercommand subCommand nick yes/no. Nick has to be in channel.Get the" +
                        " masterCommand from '${this@IRCBot.ircData.botDetails.specialChar}whichPlugin'.", conn)
            else
                SendPrivMsg(msg.retSender(), "Done setting.", conn)
        } catch(e: Throwable) {
            throw e
        }
    }

    open var commandList: List<String>
    open val aghoraPluginList = AghoraPluginsList
    open var ircData: IRCBotData

    protected lateinit var pluginManager: AghoraPluginManager
    protected var floodProtector: IRCFloodProtector

    internal val timeKeeper = AghoraTimeKeeper()

    // Used to change nicks(true = nick1 & false = nick2)
    private var nickValue = false
    // Used to update channels
    private var toUpdate = false
}