package com.github.otakusenpai.aghora.bot.bot

import com.github.otakusenpai.aghora.bot.botutils.configs.AghoraConfig
import com.github.otakusenpai.aghora.commonUtils.log.AsyncLogger
import com.github.otakusenpai.aghora.commonUtils.random
import com.github.otakusenpai.aghora.irc.connection.Connection

open abstract class AbstractBot(botConfigPath: String, serverName: String,botName: String) {

    /**
     * @return Unit
     * @since 0.1.7.1
     * This function should be called by every bot to set its connection to the server.
     */
    abstract suspend fun setConnection()

    /**
     * @return Unit
     * @since 0.1.7.1
     * This function is the bot's core loop. Implementations for specific protocols should be provided.
     */
    abstract suspend fun Connect()

    /**
     * @return Unit
     * @since 0.1.7.1
     * Disconnect the bot from the server.
     */
    fun Disconnect() {
        conn.Disconnect()
        running = false
    }

    open var botname = botName

    protected open lateinit var logger: AsyncLogger
    var server: String = serverName
    var config = AghoraConfig(botConfigPath)
    lateinit var conn: Connection
    // Used in bot loop
    var running = false
}