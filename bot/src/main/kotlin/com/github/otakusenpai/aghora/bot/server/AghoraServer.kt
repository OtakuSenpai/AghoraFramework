package com.github.otakusenpai.aghora.bot.server

import com.github.otakusenpai.aghora.bot.bot.AbstractBot
import com.github.otakusenpai.aghora.bot.bot.IRCBot
import com.github.otakusenpai.aghora.bot.botutils.configs.AghoraConfig
import com.github.otakusenpai.aghora.commonUtils.hasIt
import com.github.otakusenpai.aghora.commonUtils.log.AsyncLogger
import com.github.otakusenpai.aghora.commonUtils.log.log
import com.github.otakusenpai.aghora.commonUtils.log.logMessages
import kotlinx.coroutines.experimental.*
import kotlin.coroutines.experimental.CoroutineContext

class AghoraServer : CoroutineScope {

    constructor(configPath: String, logPath: String) {
        aghoraConfig = AghoraConfig(configPath)
        botList = aghoraConfig.getServerAndTypeList()
        logger = AsyncLogger(logPath,"server.txt")
    }

    fun addBot(bot: AbstractBot,botname: String) {
        for((serverName,botType,botName) in botList) {
            if(botName == botname) {
                if(botType == "IRC")
                    bots.add(AghoraServerData(serverName,botName,botType,bot as IRCBot))
            }
        }
    }

    fun runBots() = runBlocking {
        var errorMsg = ""
        val jobs = mutableListOf<Deferred<Unit>>()
        try {
            bots.forEach {
                it.bot.setConnection()
                delay(3000L)

                if(it.bot.running) {
                    // log("inside if", logMessages)
                    jobs.add(async(newSingleThreadContext("${it.botname}-Thread")) { it.bot.Connect() })
                    // log("done running coroutine", logMessages)
                }
            }
            var i = 0
            for(job in 0 until (jobs.size-1))
                jobs[job].await()
        } catch(e: Exception) {
            logger.log(true,false, e)

            var i = 0
            while(i <= MAX_TRIES) {
                logger.log(true,false,"Loop $i")
                if(hasIt(e.message,"irc-bot")) {
                    val name = if(errorMsg.isNotEmpty()) {
                        errorMsg.substring(errorMsg.indexOfLast
                        { it == ':' }.plus(1), errorMsg.indexOfLast { it == ' ' })
                    } else ""
                    logger.log(true,false,"name = $name")
                    val data = nameMatches("irc-bot")
                    data?.bot?.Disconnect()
                    val one = launch { data?.bot?.setConnection() }
                    one.join()
                    delay(3000L)
                    if(data?.bot!!.running) {
                        data?.bot?.Connect()
                    }
                }
            }
        }
        job = Job()
    }

    fun dispose() {
        bots.clear()
        job.cancel()
        logger.close()
    }

    private fun nameMatches(name: String): AghoraServerData? = bots.find { it.botname == name }

    // First one is server name
    // Second one is bot type
    // Third one bot name
    private var botList: List<Triple<String,String,String>>
    private var logger: AsyncLogger
    var MAX_TRIES = 5
    private var aghoraConfig: AghoraConfig
    private var bots = mutableListOf<AghoraServerData>()
    lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job
}

// First one is server name
// Second one is bot name
// Third one is bot type
// Fourth one is a Bot

data class AghoraServerData(val server: String, val botname: String, val bottype: String, val bot: AbstractBot)