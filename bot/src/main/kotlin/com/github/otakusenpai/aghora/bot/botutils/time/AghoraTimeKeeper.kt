package com.github.otakusenpai.aghora.bot.botutils.time

import com.github.otakusenpai.aghora.commonUtils.time.AghoraTime
import com.github.otakusenpai.aghora.commonUtils.time.createAghoraTime
import com.github.otakusenpai.aghora.commonUtils.time.isBetweenInclusive
import com.github.otakusenpai.aghora.irc.IRCCommands
import java.time.*
import java.time.temporal.ChronoUnit

open class AghoraTimeKeeper {
    constructor() {
        internalClock = Clock.systemDefaultZone()
    }

    internal fun initialiseTimePoint(time: Long): LocalDateTime {
        val aghoraTime = createAghoraTime(time)
        val actAfterTime = internalClock.instant().plus(aghoraTime.days,ChronoUnit.DAYS).plus(aghoraTime.hours,ChronoUnit.HOURS).plus(aghoraTime.minutes,ChronoUnit.MINUTES).plus(aghoraTime.seconds,ChronoUnit.SECONDS)

        val actTime = LocalDateTime.ofInstant(actAfterTime, ZoneId.systemDefault())
        return actTime
    }

    internal fun initialiseTimePoint(aghoraTime: AghoraTime): LocalDateTime {
        val actAfterTime = internalClock.instant().plus(aghoraTime.days,ChronoUnit.DAYS).plus(aghoraTime.hours,ChronoUnit.HOURS).plus(aghoraTime.minutes,ChronoUnit.MINUTES).plus(aghoraTime.seconds,ChronoUnit.SECONDS)

        val actTime = LocalDateTime.ofInstant(actAfterTime, ZoneId.systemDefault())
        return actTime
    }

    fun check(timeInSecs: Long, type: IRCCommands, channel: String, prefix: String, excempt: String,
              sendUnban: (channel: String, prefix: String) -> String): String {
        var retData = ""
        val checkBeforeAGT = createAghoraTime(30)
        val checkAfterAGT = createAghoraTime((3 * 60 + 30L))

        var timeToAct = initialiseTimePoint(timeInSecs)

        println("Time to act = ${timeToAct.toString()}")
        val start = LocalDateTime.ofInstant(
                Instant.now().minusSeconds(checkBeforeAGT.seconds), ZoneId.systemDefault())
        println("Start = ${start.toString()}")
        val end = LocalDateTime.ofInstant(
                Instant.now().plusSeconds(checkAfterAGT.seconds).plus(checkAfterAGT.minutes,ChronoUnit.MINUTES), ZoneId.systemDefault())
        println("End = ${end.toString()}")
        if(isBetweenInclusive(timeToAct, start, end)) {
            if(type == IRCCommands.SendUnban)
                retData = actOnTime(channel,prefix) { channel, prefix -> retData }
            else if(type == IRCCommands.SendBanwithExcempt)
                retData = actOnTime(channel, prefix,excempt) { channel, prefix, excempt -> retData }
        }

        return retData
    }

    fun check(time: AghoraTime, type: IRCCommands, channel: String, prefix: String, excempt: String,
              sendUnban: (channel: String, prefix: String) -> String,
              SendUnbanwithExcempt: (channel: String, prefix: String, excempt: String) -> String): String {
        var retData = ""
        val checkBeforeAGT = createAghoraTime(30)
        val checkAfterAGT = createAghoraTime((3 * 60 + 30L))

        var timeToAct = initialiseTimePoint(time)

        println("Time to act = ${timeToAct.toString()}")
        val start = LocalDateTime.ofInstant(
                Instant.now().minusSeconds(checkBeforeAGT.seconds), ZoneId.systemDefault())
        println("Start = ${start.toString()}")
        val end = LocalDateTime.ofInstant(
                Instant.now().plusSeconds(checkAfterAGT.seconds).plus(checkAfterAGT.minutes, ChronoUnit.MINUTES), ZoneId.systemDefault())
        println("End = ${end.toString()}")
        if (isBetweenInclusive(timeToAct, start, end)) {
            if (type == IRCCommands.SendUnban)
                retData = actOnTime(channel, prefix) { channel, prefix -> retData }
            else if (type == IRCCommands.SendUnbanwithExcempt)
                retData = actOnTime(channel, prefix, excempt) { channel, prefix, excempt -> retData }
        }

        return retData
    }

    inline fun checkTime(): Pair<Boolean,LocalDateTime> {
        val start = LocalDateTime.ofInstant(Instant.now().
                minusSeconds(60L), ZoneId.systemDefault())
        val end = LocalDateTime.ofInstant(Instant.now().plusSeconds(120L),
                ZoneId.systemDefault())

        var found = Pair(false,LocalDateTime.MIN)

        for(point in timeList)
            if(isBetweenInclusive(point, start, end)) {
                found = Pair(true,point)
            }

        return found
    }

    inline fun clear() {
        val now = LocalDateTime.now()
        val size = timeList.size
        var pos = 0

        for(i in (0..size))
            if(now.isAfter(timeList[i]))
                pos = i
        timeList.removeAt(pos)
    }

    inline fun actOnTime(channel: String, prefix: String, sendUnban: (
            channel: String, prefix: String) -> String ): String = sendUnban(channel, prefix)

    inline fun actOnTime(channel: String, prefix: String, excempt: String,
                         SendUnbanwithExcempt: (channel: String, prefix: String, excempt: String) -> String): String = SendUnbanwithExcempt(channel, prefix, excempt)

    var timeList = mutableListOf<LocalDateTime>()
    val internalClock: Clock
}