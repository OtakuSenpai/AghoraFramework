package com.github.otakusenpai.aghora.bot.botutils.plugin.callback

import com.github.otakusenpai.aghora.bot.bot.IRCBot
import com.github.otakusenpai.aghora.commonUtils.time.parseTimeInput
import com.github.otakusenpai.aghora.commonUtils.error.AghoraException
import com.github.otakusenpai.aghora.commonUtils.hasIt
import com.github.otakusenpai.aghora.commonUtils.plugin.PlgCommand
import com.github.otakusenpai.aghora.irc.IRCCommands
import com.github.otakusenpai.aghora.irc.ircmessage.IRCMessage
import com.github.otakusenpai.aghora.irc.ircmessage.isPrefix
import kotlinx.coroutines.experimental.launch


// originalMsg - the original message.
// plugin - the masterCommand of the plugin jar file(eg, basic for BasicOps)
// subCommand - eg, moo
// server - Used by setChannel and setMask
// channel - the channel the message was received from(can be a nick too)
// isChannel - If channel is nick, then false, else true
// exeIRCCommand - IRC Command to execute on call of this plugin
// params - A pair of Variable_Name to values for the options of a subCommand
// Current variable names include:
//         Prefix - The prefix to act upon
//         IRCCommands - A irc command to run
//         Excempt - A prefix to excempt from in the ban IRC Commands
//         YesOrNo - Used by the setMask and setChannel
//         Count - Used by limit
//         DescCommand - Used by desc subCommand
//         PlgCommand - Used to rerun a subcommand at a particular time
//         Time - Used to specify the time to act after, in [0..9]s/m/h/d ignore case
//         Channel - Used by some subCommands in ChanOps
//         Nick - Used by some subCommands in ChanOps
//         Message - Return message or message of any kind
data class IRCCallBack(val originalMsg: IRCMessage, val plugin: String, val subCommand: String,
                       val server: String, val channel: String, val isChannel: Boolean,
                       val exeIRCCommand: IRCCommands, val params: MutableMap<String,String>)

fun parseCommandFromPlugin(data: IRCMessage, bot: IRCBot) {
    var channel = ""
    var plugin = ""
    var subCommand = ""
    var ircCommands = IRCCommands.None
    var plgCommand = PlgCommand.none
    var isChannel = false
    var errorMsg = ""
    var params= mutableMapOf<String,String>()

    // All commands begin with ",subcommand ... etc

    try {
        var foo = launch {
            val dataList = data.retContent().split("\\s{1,}".toRegex()) as MutableList<String>


            subCommand = dataList.get(0).substring(1, dataList.get(0).length)
            channel = data.retSender()
            isChannel = !hasIt(channel, '!') && hasIt(channel, '@')

// Example plugin calls:-
// ,ban ##foo prefix - d
// ,moo - d
// ,list - d
// ,desc moo
// ,limit ##foo 30 - d
// ,invite nick ##foo (nick can be a prefix object's nick member)
// ,kick ##foo nick
// ,setChannel server ##foo allowed/denied
// ,setMask server ##foo nick!user!host plugin yes/no
// ,run moo 60s
// The fucntion should be able to know which plugin and which subCommand is calling it.

            if (subCommand == PlgCommand.moo.toString()) {
                plugin = "basic"
            } else if (subCommand == PlgCommand.list.toString()) {
                plugin = "basic"
                // todo
            } else if (subCommand == PlgCommand.setMask.toString()) {
                // todo
            } else if (subCommand == PlgCommand.setChannel.toString()) {
                // todo
            } else if (subCommand == PlgCommand.source.toString()) {
                // todo
            }
            // ,kick #foo nick
            // ,kick #foo message
            else if (subCommand == PlgCommand.kick.toString()) {
                plugin = "chan"

                if (dataList.size == 3) {
                    for (chan in bot.ircData.chanList) {
                        if (chan.retName() == dataList.get(1)) { // if channel names match
                            for (nicks in chan.userList) {
                                if (nicks.nick == dataList.get(2)) { // if nicks match
                                    params["Channel"] = chan.retName()
                                    params["Nick"] = nicks.nick
                                } else throw AghoraException("Error:  At parseCommandFromPlugin() in " +
                                        "com.otakusenpai.aghora.utils.callback: " +
                                        "Unable to find nick ${dataList.get(2)} in channel ${chan.retName()}")
                            }
                        } else throw AghoraException("Error: At parseCommandFromPlugin() in " +
                                "com.otakusenpai.aghora.utils.callback: " +
                                "My current instance isnt present in channel ${dataList.get(1)}!")
                    }
                } else if (dataList.size > 3) {
                    val tmpList = dataList.subList(3, dataList.size)
                    var msg = ""
                    var first = true
                    for (k in tmpList) {
                        if (first) {
                            msg = msg + k + " "
                            first = false
                        } else {
                            msg = msg + " " + k
                        }
                    }
                    for (chan in bot.ircData.chanList) {
                        if (chan.retName() == dataList.get(1)) { // if channel names match
                            for (nicks in chan.userList) {
                                if (nicks.nick == dataList.get(2)) { // if nicks match
                                    params["Channel"] = chan.retName()
                                    params["Nick"] = nicks.nick
                                    params["Message"] = msg
                                } else throw AghoraException("Error:  At parseCommandFromPlugin() in " +
                                        "com.otakusenpai.aghora.utils.callback: " +
                                        "Unable to find nick ${dataList.get(2)} in channel ${chan.retName()}")
                            }

                        } else throw AghoraException("Error: At parseCommandFromPlugin() in " +
                                "com.otakusenpai.aghora.utils.callback: " +
                                "My current instance isnt present in channel ${dataList.get(1)}!")
                    }
                } // kick with message

                else throw AghoraException("Error: At parseCommandFromPlugin() in " +
                        "com.otakusenpai.aghora.utils.callback: " +
                        "You are probably seeing this bcoz something went wrong!")

            } // kick

            else if(subCommand == PlgCommand.invite.toString()) {
                var found = false

                if(dataList.size < 3)
                    throw AghoraException("Error: At parseCommandFromPlugin() in " +
                            "com.otakusenpai.aghora.utils.callback: " +
                    "There is a mismatch in size got and size expected in the message!")
                val channel = dataList.get(1)
                val nick = dataList.get(2)
                for(chans in bot.ircData.chanList) {
                    if(chans.retName() == channel) {
                        params[channel] = channel
                        params[nick] = nick
                        plugin = "chan"
                        found = true
                    }
                }
                if(!found)
                    throw AghoraException("Error: At parseCommandFromPlugin() in " +
                            "com.otakusenpai.aghora.utils.callback: " +
                            "My current instance isn't present in channel $channel")
            } // invite

            else if(subCommand == PlgCommand.limit.toString()) {
                if(dataList.size < 4)
                    throw AghoraException("Error: At parseCommandFromPlugin() in " +
                            "com.otakusenpai.aghora.utils.callback: " +
                            "There is a mismatch in size got and size expected in the message!")

                val channel = dataList.get(1)
                val count = dataList.get(2)
                var found = false
                for(chans in bot.ircData.chanList) {
                    if(chans.retName() == channel) {
                        params[channel] = channel
                        params[count] = count
                        found =true
                        plugin = "chan"
                    }
                }

                if(!found)
                    throw AghoraException("Error: At parseCommandFromPlugin() in " +
                            "com.otakusenpai.aghora.utils.callback: " +
                            "I can't set limit for channel $channel since I'm not present in it!")
            } // limit

            // ",ban #channel prefix or " size = 3
            // ",ban #channel prefix except or " 4
            // ",ban #channel prefix time or "  4
            // ",ban #channel prefix except time." 5

            else if(subCommand == PlgCommand.ban.toString()) {

                if (dataList.size > 2) {
                    plugin = "chan"
                    for (i in bot.ircData.chanList) {
                        if (i.retName() == dataList.get(1)) {
                            if (dataList.size == 3) { // 1st
                                if (isPrefix(dataList.get(2))) {
                                    params["Channel"] = i.retName()
                                    params["Prefix"] = dataList.get(2)
                                    ircCommands = IRCCommands.SendUnban
                                }
                            } else if (dataList.size == 4) { // 2nd n 3rd
                                if (isPrefix(dataList.get(2))) {
                                    if (isPrefix(dataList.get(3))) { // 2nd
                                        params["Channel"] = i.retName()
                                        params["Prefix"] = dataList.get(2)
                                        params["Except"] = dataList.get(3)
                                        ircCommands = IRCCommands.SendUnbanwithExcempt
                                    } else if (parseTimeInput(dataList.get(3)).third) { // 3rd
                                        params["Channel"] = i.retName()
                                        params["Prefix"] = dataList.get(2)
                                        params["Time"] = dataList.get(3)
                                        ircCommands = IRCCommands.SendUnban
                                    }
                                }
                            } else if (dataList.size == 5) {
                                if (isPrefix(dataList.get(2)) && isPrefix(dataList.get(3)) &&
                                        parseTimeInput(dataList.get(4)).third) {
                                    params["Channel"] = i.retName()
                                    params["Prefix"] = dataList.get(2)
                                    params["Except"] = dataList.get(3)
                                    params["Time"] = dataList.get(3)
                                    ircCommands = IRCCommands.SendUnbanwithExcempt
                                }
                            }
                        }
                    }
                }
            }

        } // launch

    } catch(e: AghoraException) {
        bot.sentErrorDataTo(e.message)
    }
}

