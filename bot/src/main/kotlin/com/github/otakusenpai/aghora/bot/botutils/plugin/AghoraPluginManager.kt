package com.github.otakusenpai.aghora.bot.botutils.plugin

import com.github.otakusenpai.aghora.bot.bot.IRCBot
import com.github.otakusenpai.aghora.commonUtils.botdata.BotDetails
import com.github.otakusenpai.aghora.commonUtils.plugin.AghoraPluginsList
import com.github.otakusenpai.aghora.pluginUtils.api.AghoraInterface
import com.otakusenpai.kps.api.PluginInterface
import com.otakusenpai.kps.jar.JarProbe
import com.otakusenpai.kps.plugin.DefaultPluginManager
import java.io.File

class AghoraPluginManager() {

    constructor(plugindir: String) : this() {
        this.pluginDir = plugindir
    }

    fun loadPlugins(bot: IRCBot) {
        try {
            for((jarName, config) in bot.config.getPluginList()) {
                val probe = JarProbe(File(pluginDir + '/' + jarName))
                val pluginName  = probe.getManifestAttribute("pluginId")
                if(pluginName == null)
                    throw NullPointerException("pluginId attribute can't be null!")
                val pluginClass = probe.getManifestAttribute("pluginClass")
                if(pluginClass == null)
                    throw NullPointerException("Plugin-Class attribute can't be null!")
                val plugin = pluginManager.addPlugin(
                        pluginName,probe.file,pluginClass) as AghoraInterface
                plugin.init(config,bot.ircData)
                val masterCommand = plugin.masterCommand
                pluginList[pluginName] = masterCommand
                pluginFiles.add(File(pluginDir + '/' + jarName))
                AghoraPluginsList.populate(plugin.masterCommand,plugin.subCommands)
            }
        } catch(e: NullPointerException) {
            e.printStackTrace()
            unloadPlugins()
        }
    }

    fun unloadPlugins() {
        pluginFiles.clear()
        pluginList.clear()
        pluginManager.unloadPlugins()
    }

    /*
    fun retSubCommands(pluginName: String) {

    }
    */

    fun loadPlugin(name: String,config: String,bot: IRCBot) {
        try {
            val probe = JarProbe(File(pluginDir + '/' + name))
            val pluginName  = probe.getManifestAttribute("pluginId")
            if(pluginName == null)
                throw NullPointerException("pluginId attribute can't be null!")
            val pluginClass = probe.getManifestAttribute("pluginClass")
            if(pluginClass == null)
                throw NullPointerException("Plugin-Class attribute can't be null!")
            val plugin = pluginManager.addPlugin(
                    pluginName,probe.file,pluginClass) as AghoraInterface
            plugin.init(config, bot.ircData)
            val masterCommand = plugin.masterCommand
            pluginList[pluginName] = masterCommand
            pluginFiles.add(File(pluginDir + '/' + name))
            AghoraPluginsList.populate(plugin.masterCommand,plugin.subCommands)
        } catch(e: Throwable) {
            e.printStackTrace()
            unloadPlugins()
        }
    }

    fun unloadPlugin(pluginName: String) {
        var file = File(pluginDir + pluginName)
        pluginFiles.remove(file)
        pluginManager.removePlugin(pluginName)
        for((plgName,value) in pluginList) {
            if(pluginName == plgName) {
                pluginList.remove(plgName)
                println("Found plugin to unload: $plgName")
                AghoraPluginsList.removePlgMapItem(value)
            }
        }
        System.gc()
    }

    fun getPluginInterface(pluginName: String): PluginInterface {
        lateinit var pluginInstance: PluginInterface

        for(file in pluginFiles) {
            val probe = JarProbe(file)
            val pluginId = probe.getManifestAttribute("pluginId")
            if(pluginId == null)
                throw NullPointerException("pluginId can't be null!")
            val pluginClass = probe.getManifestAttribute("pluginClass")
            if(pluginClass == null)
                throw NullPointerException("pluginClass can't be null!")
            if(pluginId == pluginName) {
                pluginInstance = pluginManager.addPlugin(pluginName,probe.file,pluginClass)
            }
        }

        return pluginInstance
    }

    private lateinit var pluginDir: String
    var pluginFiles = mutableListOf<File>()
    val pluginManager = DefaultPluginManager()
    // first string is pluginName, like BasicOps(its the name field in the xml config)
    // second string is mastercommand,like basic(its the MasterCommand field in the xml config)
    var pluginList = mutableMapOf<String,String>()
}