Hello, and thanks for contributing to AghoraBot!

## tldr

There are three main goals in this document, depending on the nature of your pr:

- [description](#description): Please tell us about your pr.
- [checklist](#checklist): Please review the checklist that is most closly related to your pr.

The following sections provide more detail on each.

**Improve this document**

Please don't hesitate to [ask questions](issues) for clarification, or to [make suggestions](issues) (or a pull request) to improve this document.

## Description

To help the project's maintainers and community to quickly understand the nature of your pull requeset, please create a description that incorporates the following elements:

- [ ] What is accomplished by the pr
- [ ] If there is something potentially controversial in your pr, please take a moment to tell us about your choices

## Checklist

Please use the checklist that is most closely related to your pr _(you only need to use one checklist, and you can skip items that aren't applicable or don't make sense)_:

- [fixing typos]()
- [documentation]()
- [bug fix]()
- [new feature]()
- [other]()

### Fixing typos

- [ ] Please review the [readme advice]() section before submitting changes

### Documentation

- [ ] Please review the [readme advice](#readme-advice) section before submitting changes

### Bug Fix

- [ ] All existing unit tests are still passing (if applicable)
- [ ] Add new passing unit tests to cover the code introduced by your pr
- [ ] Update the readme (see [readme advice](#readme-advice))
- [ ] Update or add any necessary API documentation

### New Feature

- [ ] If this is a big feature with breaking changes, consider [opening an issue][issues] to discuss first. This is completely up to you, but please keep in mind that your pr might not be accepted.
- [ ] Run unit tests to ensure all existing tests are still passing
- [ ] Add new passing unit tests to cover the code introduced by your pr
- [ ] Update the readme (see [readme advice](#readme-advice))
Thanks for contributing!

## Readme advice

Please review this section if you are updating readme documentation.
Note: We have not yet developed the infrastructure to make docs yet! Sorry for the inconvenience! Join the IRC channel specified at [README](./README.md) to talk with the developer.

**Readme template**

Documentation will be provided in the near future. In the meantime please join the IRC channel to contribute.

**Code comments**

Please add code comments (following the same style as existing comments) to describe any code changes or new code introduced by your pull request.
