package com.github.otakusenpai.aghora.pluginUtils.api

import com.github.otakusenpai.aghora.commonUtils.plugin.AghoraPluginsList
import com.github.otakusenpai.aghora.commonUtils.plugin.AghoraRetData
import com.github.otakusenpai.aghora.irc.ircmessage.IRCMessage
import com.github.otakusenpai.aghora.otherUtils.plugins.PluginConfigHandler
import com.github.otakusenpai.aghora.pluginUtils.IRCBotData
import com.otakusenpai.kps.api.PluginInterface

// Plugin subcommand calls should be like this:-
// ${specialChar}subCommand server channel params
// Or user specific dependent on the subcommand

abstract class AghoraInterface() : PluginInterface() {
    abstract fun onCommand(data: IRCMessage): AghoraRetData
    abstract fun init(path: String,botData: IRCBotData)

    lateinit var masterCommand: String
    lateinit var subCommands: MutableList<String>
    lateinit var description: String
    lateinit var pluginConfig: PluginConfigHandler
    val pluginList = AghoraPluginsList
    lateinit var botdata: IRCBotData
}