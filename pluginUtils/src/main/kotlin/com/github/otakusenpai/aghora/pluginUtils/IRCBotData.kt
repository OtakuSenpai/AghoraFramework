package com.github.otakusenpai.aghora.pluginUtils

import com.github.otakusenpai.aghora.commonUtils.botdata.BotDetails
import com.github.otakusenpai.aghora.irc.ircmessage.Channel


// This class is used to send a bot's data to the plugins for easy working

open class IRCBotData() {

    lateinit var botDetails: BotDetails
    var chanList: MutableList<Channel> = mutableListOf()

    constructor(channelList: MutableList<Channel>,botdetails: BotDetails) : this() {
        this.botDetails = botdetails
        this.chanList = channelList
    }
}