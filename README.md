# AghoraFramework - A General Purpose Bot Framework made in Kotlin! :grin: :ok_hand:

[![License: MPL 2.0](https://img.shields.io/badge/License-MPL%202.0-brightgreen.svg)](https://opensource.org/licenses/MPL-2.0)

Welcome to AghoraFramework, a general purpose bot framework made in Kotlin!!! 😉

Creating your own bots is very easy. For example,
```
class MyBot1(configPath: String,serverName: String,
               botName: String) : IRCBot(configPath,serverName,botName) {

    override suspend fun customMsgHandlers(data: MutableList<IRCMessage>) {
        try {
            for(msg in data) {
                val tempContent = if(hasIt(msg.retContent(),':')) {
                    msg.retContent().substring(msg.retContent().indexOf(':')+1,
                            msg.retContent().length)
                } else {
                    msg.retContent()
                }
                if(beginsWith(tempContent,"mwaaa") || beginsWith(tempContent,"mwaa"))
                    SendPrivMsg(msg.retSender(),"mwaaa!!!",conn)
            }
        } catch(e: Throwable) {
            e.printStackTrace()
        }
    }
}
```

"configPath" is the path to the framework's config file. "serverName" and "botName" are variables you set in the config's portion of this bot's settings.
The function customMsgHandlers(data: MutableList<IRCMessage>) is a abstract function that should be defined for all overrides of IRCBot class. Use this to make custom commands for the bot.

Next, create a AghoraServer object.
```
val server = AghoraServer(configPath,"./logs/server/")
```
The first parameter is the path to the framework's config, which the second one is the directory path where the server object stores debuggin errors and other stuff.

Next, initialise the bot object:
```
val myBot = TestBot(configPath,"Freenode","myBot")
```
Then we add this bot object to the server.
```
server.addBot(myBot as AbstractBot,myBot.botname)
```
This adds the bot and its metadata to a list of Triples and a data class which helps in doing the next step.
```
server.runBots()
server.dispose()
```
The first line uses the metadata and AbstractBot object to launch different types of bots, like Discord bots or Telegram bots.
All these bots are launched in their own thread, which runs asynchronously with proper context.

Currently only a IRC bot is implemented using the internal IRC library. In future revisions(possibly v0.3) I'll add a Discord bot too.

The main method for my testbot is [here](https://github.com/OtakuSenpai/AghoraFramework/blob/v0.1/src/main/kotlin/com/github/otakusenpai/aghora/Main.kt) for reference.

Hope you like it :sunglasses: :blush:

## Build n Run

 Check these [useful tips](./RUN.md).

## Todo

I plan to add a lot of things. Here's a few of them:

  ### Framework Specific

    #### In Progress
      * Get lots of plugins for it - WIP
      * Make the bot connect to discord,slack and telegram - WIP
      * Make the server class more versatile, like relaunching a Bot interface when stopped or thrown an exception from - WIP
      * Make the bot's config system more versatile - WIP
      * Make a GUI for managing different connections of the bot server - In later versions

    #### Done
      * Separate the bot's different classes(from bot,irc and utils in core module) into their own modules - Done

  ### IRC Library

    ## In Progress
      * Add support for all RFC 2812 message types(server and client) - WIP
      * Separate the IRC parsing part into its own library outside the project, under MIT license - WIP
      * Adding better plugin support for the IRC bots, and possible change to Pf4J - WIP
      * Add support for IRCv3 capabilities - In later versions
      * Add support for SASL (plain, at first) - In later versions
      * Add support for mIrc colors - In later versions
      * Add support for different ircds other than Freenode's(like ratbox and others) - In later versions
      * Add CTCP support - In later versions
      * Add DCC support - In later versions


## Contributing and the Community

If you want to contribute to AghoraBot, thanks! See [PULL_REQUEST_TEMPLATE.md](https://github.com/OtakuSenpai/AghoraBot/blob/master/PULL_REQUEST_TEMPLATE.md) and [CONTRIBUTING.md](https://github.com/OtakuSenpai/AghoraBot/blob/master/CONTRIBUTING.md) for more details.
Also don't forget to join our IRC channel on [Freenode](https://kiwiirc.com/nextclient/chat.freenode.net/#AghoraBot)

## FAQs

### Why is the development so erratic?
I'm a college going student and my daily grinding at college chores and other stuff like playing games might disable me from working on the bot full time. But the bot is in active evelopment as of now and I do regualar code additions depnding on my free time. Just that I don't commit frequently doesnt mean I have left developing this project.

### How do I get in touch with the lead developer?
You can join the [IRC channel](https://kiwiirc.com/nextclient/chat.freenode.net/#AghoraBot). Otherwise make a issue on github, I'll respond.

### Why is there no wiki or javadoc?
I haven't added any wiki yet, because I barely having time to do stuff, and this is causing me to code less on the project.

## License

 Copyright @ Avra Neel Chakraborty under the Mozilla Public License, Version 2.0.(See accompanying file [`LICENSE`](./LICENSE) or get a copy at https://www.mozilla.org/en-US/MPL/2.0/)

