---
name: Plugin Feature request
about: What kind of plugin could you like to add?

---

# Basic Information

  Hey! You have been using this bot for a while, and thought of some cool plugin idea. How about you ask the project maintainer/s issue about this plugin.

  Every plugin consists of a mastercommand which has a list subcommands. Here by subcommands, we mean a group of "words" which when the bot receives them, it carries some task and has similar use case and perhaps similar dependencies, traits,etc which charactify the mastercommand. So if your idea of a subcommand/plugin has a high chance of being in a existing group of plugins, then state it. Otherwise just apply for a new plugin mastercommand.

  Currently only IRC plugins are accepted. In future that might change.

## Specific Information

  ### Plugin Group Name

    The name of the plugin mastercommand. Keep it short, one word.

  ### Plugin SubCommand List

  All the sbcommands in a plugin,like for example(state this in a list):-

  * foo - Prints the output foo
  * ping - Pings a user

  Please state all the subcommands you wish to add for this plugin, and be specific on what each of the subcommands does.
  Also state if a subcommand can only be accessed by the owner, or set to all by access list.

  ### Type of Plugin

  If the plugin is textual, or does network queries, or other.
