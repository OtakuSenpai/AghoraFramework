---
name: 'Bot Feature Addition '
about: Addition of new features to the bot framework.

---

# Bot Feature Info

  Hey, if you like this framework and have been using it for sometime now, and want the framework to do more, then this is the place to add that stuff.

## In Details

### Part of the framework to work on

  The part of the bot framework which needs more work on.

### What needs to be done

  What needs to be done, according to you.

### Bot Feature Benefits

  How will the bot benefit from this feature?


### Further ideas ...

  More stuff if you want to add (OPTIONAL).
