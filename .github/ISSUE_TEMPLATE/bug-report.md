---
name: Bug Report
about: A report on a bug in the bot framework

---

# The following information is required(please fill the info in the appropriate place) :-

  ## Description of the bug

    A clear and concise description of what the bug is, including scope of the attack(causes crash or    otherwise,etc).

  ## How to Reproduce

    What steps are needed to reproduce the bug locally.

  ## Error Log

    A stacktrace of the bug.

# Extra

## Expected behavior

  A clear and concise description of what you expected to happen.

## Screenshots

If applicable, add screenshots to help explain your problem.

## More Info

 - OS [e.g. Linux/MacOS/Windows]
 - Version
 - Chat server info :-
   * If IRC, IRCD and server
   * If Discord, error line
